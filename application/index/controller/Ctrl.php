<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;

class Ctrl extends Base
{
      //錢包頁面
    public function wallet()
    {
        $balance = db('xy_users')->where('id',session('user_id'))->value('balance');
        $this->assign('balance',$balance);
        $jing_balance = db('xy_users')->where('id',session('user_id'))->value('jing_balance');
        $this->assign('jing_balance',$jing_balance);
        $dong_balance = db('xy_users')->where('id',session('user_id'))->value('dong_balance');
        $this->assign('dong_balance',$dong_balance);
        $balanceT = db('xy_convey')->where('uid',session('user_id'))->where('status',1)->sum('commission');
        $this->assign('balance_shouru',$balanceT);

        //收益
        $startDay = strtotime( date('Y-m-d 00:00:00', time()) );
        $shouyi = db('xy_convey')->where('uid',session('user_id'))->where('addtime','>',$startDay)->where('status',1)->select();

        //充值
        $chongzhi = db('xy_recharge')->where('uid',session('user_id'))->select();

        //提現
        $tixian = db('xy_deposit')->where('uid',session('user_id'))->select();

        $this->assign('shouyi',$shouyi);
        $this->assign('chongzhi',$chongzhi);
        $this->assign('tixian',$tixian);
        return $this->fetch();
    }


    public function recharge_before()
    {
        $pay = db('xy_pay')->where('status',1)->select();

        $this->assign('pay',$pay);
        return $this->fetch();
    }

    public function vipsj()
    {
        $jb=$_POST['level'];
        $uids1 = model('admin/Users')->child_user(session('user_id'),1,0);
        $yyrs = count($uids1);
        $yqrs = db('xy_level')->where('level',$jb)->value('auto_vip_xu_num');
       if($yyrs >= $yqrs){
         $sj=db('xy_users')->where('id',session('user_id'))->update(['level'=>$jb]);  
          
         $this->assign('data','解鎖成功');

       }else{
           
         $this->assign('data',"解鎖失敗");  
       }
       
       
        return $this->fetch();
}

    public function vip()
    {
        $pay = db('xy_pay')->where('status',1)->select();
        $this->member_level = db('xy_level')->order('level asc')->select();;
        $this->info = db('xy_users')->where('id', session('user_id'))->find();
        $this->member = $this->info;

        $level_name = $this->member_level[0]['name'];
        $order_num = $this->member_level[0]['order_num'];
        if (!empty($this->info['level'])){
            $level_name = db('xy_level')->where('level',$this->info['level'])->value('name');;
        }
        if (!empty($this->info['order_num'])){
            $order_num = db('xy_level')->where('level',$this->info['level'])->value('order_num');;
        }

        $uids1 = model('admin/Users')->child_user(session('user_id'),1,0);
        $this->zhitui = count($uids1);
        $this->level_name = $level_name;
        $this->order_num = $order_num;
        $this->list = $pay;
        return $this->fetch();
    }

    /**
     * @地址      recharge_dovip
     * @說明      利息寶
     * @參數       @參數 @參數
     * @返回      \think\response\Json
     */
    public function lixibao()
    {
        $this->assign('title','利息寶');
        $uinfo = db('xy_users')->field('username,tel,level,id,headpic,balance,freeze_balance,lixibao_balance,lixibao_dj_balance')->find(session('user_id'));
        
		$recharge = Db::name('xy_recharge')->where('uid',session('user_id'))->where('status',2)->count();
        
		
		if($recharge < 1){
			$balance = '0.00';
		}else{
			$balance = $uinfo['balance'];
		}
        $this->assign('ubalance',$balance);
        $this->assign('balance',$uinfo['lixibao_balance']);
        $this->assign('balance_total',$uinfo['lixibao_balance'] + $uinfo['lixibao_dj_balance']);
        $balanceT = db('xy_lixibao')->where('uid',session('user_id'))->where('status',1)->where('type',3)->sum('num');

        $balanceT = db('xy_balance_log')->where('uid',session('user_id'))->where('status',1)->where('type',23)->sum('num');

        $yes1 = strtotime( date("Y-m-d 00:00:00",strtotime("-1 day")) );
        $yes2 = strtotime( date("Y-m-d 23:59:59",strtotime("-1 day")) );
        $this->yes_shouyi = db('xy_balance_log')->where('uid',session('user_id'))->where('status',1)->where('type',23)->where('addtime','between',[$yes1,$yes2])->sum('num');

        $this->assign('balance_shouru',$balanceT);


        //收益
        $startDay = strtotime( date('Y-m-d 00:00:00', time()) );
        $shouyi = db('xy_lixibao')->where('uid',session('user_id'))->select();

        foreach ($shouyi as &$item) {
            $type = '';
            if ($item['type'] == 1) {
                $type = '<font color="green">轉入利息寶</font>';
            }elseif ($item['type'] == 2) {
                $n = $item['status'] ? '已到賬' : '未到賬';
                $type = '<font color="red" >利息寶轉出('.$n.')</font>';
            }elseif ($item['type'] == 3) {
                $type = '<font color="orange" >每日收益</font>';
            }else{

            }

            $lixbao = Db::name('xy_lixibao_list')->find($item['sid']);

            $name = $lixbao['name'].'('.$lixbao['day'].'天)'.$lixbao['bili']*100 .'% ';

            $item['num'] = number_format($item['num'],2);
            $item['name'] = $type.'　　'.$name;
            $item['shouxu'] = $lixbao['shouxu']*100 .'%';
            $item['addtime'] = date('Y/m/d H:i', $item['addtime']);

            if ($item['is_sy'] == 1) {
                $notice = '正常收益,實際收益'.$item['real_num'];
            }else if ($item['is_sy'] == -1) {
                $notice = '未到期提前提取,未收益,手續費為:'.$item['shouxu'];
            }else{
                $notice = '理財中...';
            }
            $item['notice'] =$notice;
        }

        $this->rililv = config('lxb_bili')*100 .'%' ;
        $this->shouyi=$shouyi;
        if(request()->isPost()) {
            return json(['code'=>0,'info'=>'操作','data'=>$shouyi]);
        }

        $lixibao = Db::name('xy_lixibao_list')->field('id,name,bili,day,min_num')->order('day asc')->select();
        $this->lixibao = $lixibao;
        return $this->fetch();
    }

    public function lixibao_ru()
    {
        $uid = session('user_id');
        $uinfo = Db::name('xy_users')->field('recharge_num,deal_time,balance,level')->find($uid);//獲取用戶今日已充值金額

        if(request()->isPost()){
            $price = input('post.price/d',0);
            $id = input('post.lcid/d',0);
            $yuji=0;
            if ($id) {
                $lixibao = Db::name('xy_lixibao_list')->find($id);
                if ($price < $lixibao['min_num']) {
                    return json(['code'=>1,'info'=>'該產品最低起投金額'.$lixibao['min_num']]);
                }
                if ($price > $lixibao['max_num']) {
                    return json(['code'=>1,'info'=>'該產品最高可投金額'.$lixibao['max_num']]);
                }
                $yuji = $price * $lixibao['bili'] * $lixibao['day'];
            }else{
                return json(['code'=>1,'info'=>'數據異常']);
            }


            if ( $price <= 0 ) {
                return json(['code'=>1,'info'=>'you are sb']); //直接充值漏洞
            }
            $recharge = Db::name('xy_recharge')->where('uid',session('user_id'))->where('status',2)->count();
			if($recharge < 1){
				$balance = 0;
			}else{
				$balance = $uinfo['balance'];
			}
            if ($balance < $price ) {
                return json(['code'=>1,'info'=>'可用餘額不足']);
            }
            Db::name('xy_users')->where('id',$uid)->setInc('lixibao_balance',$price);  //利息寶月 +
            Db::name('xy_users')->where('id',$uid)->setDec('balance',$price);  //餘額 -

            $endtime = time() + $lixibao['day'] * 24 * 60 * 60;

            $res = Db::name('xy_lixibao')->insert([
                'uid'         => $uid,
                'num'         => $price,
                'addtime'     => time(),
                'endtime'     => $endtime,
                'sid'         => $id,
                'yuji_num'         => $yuji,
                'type'        => 1,
                'status'      => 0,
            ]);
            $oid = Db::name('xy_lixibao')->getLastInsID();
            $res1 = Db::name('xy_balance_log')->insert([
                //記錄返傭信息
                'uid'       => $uid,
                'oid'       => $oid,
                'num'       => $price,
                'type'      => 21,
                'addtime'   => time()
            ]);
            if($res) {
                return json(['code'=>0,'info'=>'操作成功']);
            }else{
                return json(['code'=>1,'info'=>'操作失敗!請檢查賬號餘額']);
            }
        }

        $this->rililv = config('lxb_bili')*100 .'%' ;
        $this->yue = $uinfo['balance'];
        $isajax = input('get.isajax/d',0);
        
        if ($isajax) {
            $lixibao = Db::name('xy_lixibao_list')->field('id,name,bili,day,min_num')->select();
            $data2=[];
            $str = $lixibao[0]['name'].'('.$lixibao[0]['day'].'天)'.$lixibao[0]['bili']*100 .'% ('.$lixibao[0]['min_num'].'起投)';
            foreach ($lixibao as $item) {
                $data2[] = array(
                    'id'=>$item['id'],
                    'value'=>$item['name'].'('.$item['day'].'天)'.$item['bili']*100 .'% ('.$item['min_num'].'起投)',
                );
            }
            return json(['code'=>0,'info'=>'操作','data'=>$data2,'data0'=>$str]);
        }

        $this->libi =1;

        $this->assign('title','利息寶餘額轉入');
        return $this->fetch();
    }


    public function deposityj()
    {
        $num = input('post.price/f',0);
        $id = input('post.lcid/d',0);
        if ($id) {
            $lixibao = Db::name('xy_lixibao_list')->find($id);

            $res = $num * $lixibao['day'] * $lixibao['bili'];
            return json(['code'=>0,'info'=>'操作','data'=>$res ,'data1'=>$id]);
        }
    }

    public function lixibao_chu()
    {
        $uid = session('user_id');
        $uinfo = Db::name('xy_users')->field('recharge_num,deal_time,balance,level,lixibao_balance')->find($uid);//獲取用戶今日已充值金額

        if(request()->isPost()){
            $id = input('post.id/d',0);
            $lixibao = Db::name('xy_lixibao')->find($id);
            if (!$lixibao) {
                return json(['code'=>1,'info'=>'數據異常']);
            }
            if ($lixibao['is_qu']) {
                return json(['code'=>1,'info'=>'重復操作']);
            }
            $price = $lixibao['num'];

            if ($uinfo['lixibao_balance'] < $price ) {
                return json(['code'=>1,'info'=>'可用餘額不足']);
            }
            //利息寶參數
            $lxbParam = Db::name('xy_lixibao_list')->find($lixibao['sid']);

            //
            $issy = 0;
            if ( time() > $lixibao['endtime'] ) {
                //未到期
                $issy= 1;
            }else{
                $issy= -1;
            }

            Db::name('xy_users')->where('id',$uid)->setDec('lixibao_balance',$price);  //餘額 -

            $oldprice = $price;
            $shouxu = $lxbParam['shouxu'];
            if ($shouxu) {
                $price = $price - $price*$shouxu;
            }
            
            $res = Db::name('xy_lixibao')->where('id',$id)->update([
                'endtime'     => time(),
                'is_qu'      => 1,
                'is_sy'      => $issy,
                'shouxu'     =>$oldprice*$shouxu
            ]);


            Db::name('xy_users')->where('id',$uid)->setInc('balance',$price);  //餘額 +
            $res1 = Db::name('xy_balance_log')->insert([
                //記錄返傭信息
                'uid'       => $uid,
                'oid'       => $id,
                'num'       => $price,
                'type'      => 22,
                'addtime'   => time()
            ]);

            //利息寶記錄轉出


            if($res) {
                return json(['code'=>0,'info'=>'操作成功']);
            }else{
                return json(['code'=>1,'info'=>'操作失敗!請檢查賬號餘額']);
            }

        }

        $this->assign('title','利息寶餘額轉出');
        $this->rililv = config('lxb_bili')*100 .'%' ;
        $this->yue = $uinfo['lixibao_balance'] ;
        $this->s = $s=time();
        $log = $this->_query('xy_lixibao')->where('uid',session('user_id'))->order('addtime desc')->page();


        return $this->fetch();
    }



    //升級vip
    public function recharge_dovip()
    {
        if(request()->isPost()){
            $level = input('post.level/d',1);
            $type = input('post.type/s','');

            $uid = session('user_id');
            $uinfo = db('xy_users')->field('pwd,salt,tel,username,balance')->find($uid);
            if(!$level ) return json(['code'=>1,'info'=>'參數錯誤']);

            //
            $pay = db('xy_pay')->where('id',$type)->find();
            $num = db('xy_level')->where('level',$level)->value('num');;

            if ($num > $uinfo['balance']) {
                return json(['code'=>1,'info'=>'可用餘額不足!']);
            }



            $id = getSn('SY');
            $res = db('xy_recharge')
                ->insert([
                    'id'        => $id,
                    'uid'       => $uid,
                    'tel'       => $uinfo['tel'],
                    'real_name' => $uinfo['username'],
                    'pic'       => '',
                    'num'       => $num,
                    'addtime'   => time(),
                    'pay_name'  => $type,
                    'is_vip'    => 1,
                    'level'     =>$level,
					'bank'     =>'',
					'bankname'     =>''
                ]);
            if($res){
                if ($type == 999) {
                    $res1 = Db::name('xy_users')->where('id',$uid)->update(['level'=>$level]);
                    $res1 = Db::name('xy_users')->where('id',$uid)->setDec('balance',$num);
                    $res = Db::name('xy_recharge')->where('id',$id)->update(['endtime'=>time(),'status'=>2]);


                    $res2 = Db::name('xy_balance_log')
                        ->insert([
                            'uid'=>$uid,
                            'oid'=>$id,
                            'num'=>$num,
                            'type'=>1,
                            'status'=>1,
                            'addtime'=>time(),
                        ]);
                    return json(['code'=>0,'info'=>'升級成功']);
                }



                $pay['id'] = $id;
                $pay['num'] =$num;
                if ($pay['name2'] == 'bipay' ) {
                    $pay['redirect'] = url('/index/Api/bipay').'?oid='.$id;
                }
                if ($pay['name2'] == 'paysapi' ) {
                    $pay['redirect'] = url('/index/Api/pay').'?oid='.$id;
                }

                if ($pay['name2'] == 'card' ) {
                    $pay['master_cardnum']= config('master_cardnum');
                    $pay['master_name']= config('master_name');
                    $pay['master_bank']= config('master_bank');
                }

                return json(['code'=>0,'info'=>$pay]);
            }

            else
                return json(['code'=>1,'info'=>'提交失敗，請稍後再試']);
        }
        return json(['code'=>0,'info'=>'請求成功!','data'=>[]]);
    }


    public function recharge(){
        $uid = session('user_id');
        $tel = Db::name('xy_users')->where('id',$uid)->value('tel');//獲取用戶今日已充值金額
        $this->tel = substr_replace($tel,'****',3,4);

        return $this->fetch();
    }

    public function recharge_do_before()
    {
        $num = input('post.price/f',0);
        $type = input('post.type/s','card');

        $uid = session('user_id');
        if(!$num ) return json(['code'=>1,'info'=>'參數錯誤']);

        //時間限制 //TODO
        $res = check_time(config('chongzhi_time_1'),config('chongzhi_time_2'));
        $str = config('chongzhi_time_1').":00  - ".config('chongzhi_time_2').":00";
        if($res) return json(['code'=>1,'info'=>'禁止在'.$str.'以外的時間段執行當前操作!']);


        //
        $pay = db('xy_pay')->where('name2',$type)->find();
        if ($num < $pay['min']) return json(['code'=>1,'info'=>'充值不能小於'.$pay['min']]);
        if ($num > $pay['max']) return json(['code'=>1,'info'=>'充值不能大於'.$pay['max']]);

        $info = [];
        $info['num'] = $num;
        return json(['code'=>0,'info'=>$info]);
    }



    public function recharge2()
    {
        $oid = input('get.oid/s','');
        $num = input('get.num/s','');
        if(request()->isPost()) {
            $id = input('post.id/s', '');
            $pic = input('post.pic/s', '');

            if (is_image_base64($pic)) {
                $pic = '/' . $this->upload_base64('xy', $pic);  //調用圖片上傳的方法
            }else{
                return json(['code'=>1,'info'=>'圖片格式錯誤']);
            }

            $res = db('xy_recharge')->where('id',$id)->update(['pic'=> $url]);
            if (!$res) {
                return json(['code'=>1,'info'=>'提交失敗，請稍後再試']);
            }else{
                return json(['code'=>0,'info'=>'請求成功!','data'=>[]]);
            }
        }
		$banklist = db('xy_bank_list')->order('cs asc')->limit(1)->find();//dump($banklist);die;
		$this->assign('banklist',$banklist);
        //$num = $num.'.'.rand(10,99); //隨機金額
        $info = [];//db('xy_recharge')->find($oid);
        $info['nnum'] = round($num/30);//db('xy_recharge')->find($oid);
        $info['num'] = $num;
        $info['master_bank'] = config('master_bank');//銀行名稱
        $info['master_name'] = config('master_name');//收款人
        $info['master_cardnum'] = config('master_cardnum');//銀行卡號
        $info['master_bk_address'] = config('master_bk_address');//銀行地址
        
        $info['master_bank2'] = config('master_bank2');//銀行名稱
        $info['master_name2'] = config('master_name2');//收款人
        $info['master_cardnum2'] = config('master_cardnum2');//銀行卡號
        $info['master_bk_address2'] = config('master_bk_address2');//銀行地址
        
        $info['master_bank3'] = config('master_bank3');//銀行名稱
        $info['master_name3'] = config('master_name3');//收款人
        $info['master_cardnum3'] = config('master_cardnum3');//銀行卡號
        $info['master_bk_address3'] = config('master_bk_address3');//銀行地址
        
        
        
        
        $this->info = $info;

        return $this->fetch();
    }

    //三方支付
    public function recharge3()
    {

        $type = isset($_REQUEST['type']) ? $_REQUEST['type']: 'wx';
        $pay = db('xy_pay')->where('status',1)->select();
        $this->assign('title',$type=='wx' ? '微信支付': '支付寶支付');
        $this->assign('pay',$pay);
        $this->assign('type',$type);
        return $this->fetch();
    }


    //錢包頁面
    public function bank()
    {
        $balance = db('xy_users')->where('id', session('user_id'))->value('balance');
        $this->assign('balance', $balance);
        $balanceT = db('xy_convey')->where('uid', session('user_id'))->where('status', 2)->sum('commission');
        $this->assign('balance_shouru', $balanceT);
        return $this->fetch();
    }

    //獲取提現訂單介面
    public function get_deposit()
    {
        $info = db('xy_deposit')->where('uid',session('user_id'))->select();
        if($info) return json(['code'=>0,'info'=>'請求成功','data'=>$info]);
        return json(['code'=>1,'info'=>'暫無數據']);
    }

    public function my_data()
    {
        $uinfo = db('xy_users')->where('id', session('user_id'))->find();
        if ($uinfo['tel']) {
            $uinfo['tel'] = substr_replace($uinfo['tel'], '****', 3, 4);
        }
        $bank = db('xy_bankinfo')->where(['uid'=>session('user_id')])->find();
        $uinfo['cardnum'] = substr_replace($bank['cardnum'],'****',7,7);
        if(request()->isPost()) {
            $username = input('post.username/s', '');
            //$pic = input('post.qq/s', '');

            $res = db('xy_users')->where('id',session('user_id'))->update(['username'=>$username]);
            if (!$res) {
                return json(['code'=>1,'info'=>'提交失敗，請稍後再試']);
            }else{
                return json(['code'=>0,'info'=>'請求成功!','data'=>[]]);
            }
        }

        $this->assign('info', $uinfo);

        return $this->fetch();
    }



    public function recharge_do()
    {
        if(request()->isPost()){
            $num = input('post.price/f',0);
            $pic = input('post.pic/s','');
            //解碼
            $tmp  = base64_decode(substr($pic,23));
            //寫文件
            $tmpStr = 'family'.time().rand(100,9999);
            $pic_path='upload/family/'.$tmpStr.'.jpg';
            file_put_contents($pic_path,$tmp);
			//$bank =   input('post.bank');
			$bank_name =  input('post.bank_name');
            $uid = session('user_id');
            $uinfo = db('xy_users')->field('pwd,salt,tel,username')->find($uid);
            if(!$num ) return json(['code'=>1,'info'=>'參數錯誤']);
            //$pay = db('xy_pay')->where('name2',$type)->find();
            //if ($num < $pay['min']) return json(['code'=>1,'info'=>'充值不能小於'.$pay['min']]);
            //if ($num > $pay['max']) return json(['code'=>1,'info'=>'充值不能大於'.$pay['max']]);

            $id = getSn('SY');
            $res = db('xy_recharge')
                ->insert([
                    'id'        => $id,
                    'uid'       => $uid,
                    'tel'       => $uinfo['tel'],
                    'real_name' => $uinfo['username'],
                    'pic'       => $pic_path,
                    'num'       => $num,
                    'addtime'   => time(),
                    'pay_name'  => $bank_name,
                ]);
            if($res){
                $rechargecs = db('xy_bank_list')->where('bank_name',$bank_name)->setInc('cs',1);
            }

            else
                return json(['code'=>1,'info'=>'提交失敗，請稍後再試']);
        }
        return json(['code'=>0,'info'=>'請求成功!','data'=>[]]);
    }

    function deposit_wx(){

        $user = db('xy_users')->where('id', session('user_id'))->find();
        $this->assign('title','微信提現');

        $this->assign('type','wx');
        $this->assign('user',$user);
        return $this->fetch();
    }

    function deposit(){

        $user = db('xy_users')->where('id', session('user_id'))->find();
        $user['tel'] = substr_replace($user['tel'],'****',3,4);
        $bank = db('xy_bankinfo')->where(['uid'=>session('user_id')])->find();
		$recharge = db('xy_recharge')->where('uid',session('user_id'))->where('status',2)->count();
		if($recharge < 1){
			$balance = '0.00';
		}else{
			$balance = $user['balance'];
		}
        $bank['cardnum'] = substr_replace($bank['cardnum'],'**** **** **** ',0,4);
        $this->assign('info',$bank);
        $this->assign('user',$user);
        $this->assign('balance',$balance);

        //提現限制
        $level = $user['level'];
        !$user['level'] ? $level = 0 : '';
        $ulevel = Db::name('xy_level')->where('level',$level)->find();
        $this->shouxu = $ulevel['tixian_shouxu'];;;

        return $this->fetch();
    }
    function deposit_zfb(){

        $user = db('xy_users')->where('id', session('user_id'))->find();
        $this->assign('title','支付寶提現');

        $this->assign('type','zfb');
        $this->assign('user',$user);
        return $this->fetch('deposit_zfb');
    }


    //提現介面
    public function do_deposit()
    {
        $res = check_time(config('tixian_time_1'),config('tixian_time_2'));
        $str = config('tixian_time_1').":00  - ".config('tixian_time_2').":00";
        if($res) return json(['code'=>1,'info'=>'禁止在'.$str.'以外的時間段執行當前操作!']);


        $bankinfo = Db::name('xy_bankinfo')->where('uid',session('user_id'))->where('status',1)->find();
        //var_dump($bankinfo);die;
        $type = input('post.type/s','');

        if ($type ==  'wx' || $type == 'zfb' ){

        }else{
            if(!$bankinfo) return json(['code'=>1,'info'=>'還沒添加USDT地址!']);
        }


        if(request()->isPost()){
            $uid = session('user_id');
			$wallet_type = input('post.wallet_type','');//錢包選擇
            //交易密碼
            $pwd2 = input('post.paypassword/s','');
            $info = db('xy_users')->field('pwd2,salt2')->find(session('user_id'));
            if($info['pwd2']=='') return json(['code'=>1,'info'=>'未設置交易密碼']);
            if($info['pwd2']!=sha1($pwd2.$info['salt2'].config('pwd_str'))) return json(['code'=>1,'info'=>'密碼錯誤']);
            $num = input('post.num/d',0);
            $bkid = input('post.bk_id/d',$bankinfo['id']);
            $token = input('post.token','');
            $data = ['__token__' => $token];
            $validate   = \Validate::make($this->rule,$this->msg);
            if(!$validate->check($data)) return json(['code'=>1,'info'=>$validate->getError()]);

            if ($num <= 0)return json(['code'=>1,'info'=>'參數錯誤']);

            $uinfo = Db::name('xy_users')->field('recharge_num,deal_time,balance,level,jing_balance,dong_balance')->find($uid);//獲取用戶今日已充值金額
            
            //提現限制
            $level = $uinfo['level'];
            !$uinfo['level'] ? $level = 0 : '';
            $ulevel = Db::name('xy_level')->where('level',$level)->find();
            if ($num < $ulevel['tixian_min']) {
                return ['code'=>1,'info'=>'會員等級 提現額度為 '.$ulevel['tixian_min'].'-'.$ulevel['tixian_max'].'!'];
            }
            if ($num >= $ulevel['tixian_max']) {
                return ['code'=>1,'info'=>'會員等級 提現額度為 '.$ulevel['tixian_min'].'-'.$ulevel['tixian_max'].'!'];
            }

            $onum =  db('xy_convey')->where('uid',$uid)->where('addtime','between',[strtotime(date('Y-m-d')),time()])->count('id');
            if ($onum < $ulevel['tixian_nim_order']) {
                return ['code'=>1,'info'=>'當前等級提現需完成  '.$ulevel['tixian_nim_order'].' 筆訂單!'];
            }
            
            //選擇錢包提示
			if($wallet_type == 3){
				//$txjing = db('xy_deposit')->where('uid',$uid)->where('type',2)->count('num');
				//$ktxbalance = $uinfo['balance'] - $txjing;
				$recharge = Db::name('xy_recharge')->where('uid',$uid)->where('status',2)->count();
				if($recharge < 1){
					$balance = 0;
				}else{
					$balance = $uinfo['balance'];
				}
	            if ($num > $balance) return json(['code'=>1,'info'=>'本金錢包可提現餘額不足！']);
            	$type = 3;
			}elseif($wallet_type == 2){
				if ($num > $uinfo['dong_balance']) return json(['code'=>1,'info'=>'動態錢包可提現餘額不足！']);
            	$type = 2;
			}else{
				if ($num > $uinfo['jing_balance']) return json(['code'=>1,'info'=>'靜態錢包可提現餘額不足！']);
            	$type = 1;
			}

            if($uinfo['deal_time']==strtotime(date('Y-m-d'))){
                //提現次數限制
                $tixianCi = db('xy_deposit')->where('uid',$uid)->where('addtime','between',[strtotime(date('Y-m-d 00:00:00')),time()])->count();
                if ($tixianCi+1 > $ulevel['tixian_ci'] ) {
                    return ['code'=>1,'info'=>'提現已達到上限!'];
                }

            }else{
                //重置最後交易時間
                Db::name('xy_users')->where('id',$uid)->update(['deal_time'=>strtotime(date('Y-m-d')),'deal_count'=>0,'recharge_num'=>0,'deposit_num'=>0]);
            }
            $id = getSn('CO');
            try {
                Db::startTrans();
                $res = Db::name('xy_deposit')->insert([
                    'id'          => $id,
                    'uid'         => $uid,
                    'bk_id'       => $bkid,
                    'num'         => $num,
                    'addtime'     => time(),
                    'type'        => $type,
                    'shouxu'      => $ulevel['tixian_shouxu'],
                    'real_num'    => $num - ($num*$ulevel['tixian_shouxu'])
                ]);

                //提現日誌
                $res2 = Db::name('xy_balance_log')
                    ->insert([
                        'uid' => $uid,
                        'oid' => $id,
                        'num' => $num,
                        'type' => 7, //TODO 7提現
                        'status' => 2,
                        'addtime' => time(),
                    ]);

				
				if($wallet_type == 3){
					//$txjings = Db::name('xy_deposit')->where('uid',$uid)->where('type',1)->sum('num');
					//$date['jing_balance'] = 0;
					//$date['dong_balance'] = 0;
					//$date['balance'] = $uinfo['balance'] - $num - $txjings - $uinfo['jing_balance'];
		            $res1 = Db::name('xy_users')->where('id',session('user_id'))->setDec('balance',$num);
				}elseif($wallet_type == 2){
					$res1 = Db::name('xy_users')->where('id',session('user_id'))->setDec('dong_balance',$num);
				}else{
					$res1 = Db::name('xy_users')->where('id',session('user_id'))->setDec('jing_balance',$num);
				}
                
                if($res && $res1){
                    Db::commit();
                    return json(['code'=>0,'info'=>'操作成功!']);
                }else{
                    Db::rollback();
                    return json(['code'=>1,'info'=>'操作失敗!']);
                }
            } catch (\Exception $e){
                Db::rollback();
                return json(['code'=>1,'info'=>'操作失敗!請檢查賬號餘額']);
            }
        }
        return json(['code'=>0,'info'=>'請求成功!','data'=>$bankinfo]);
    }

    //////get請求獲取參數，post請求寫入數據，post請求傳人bkid則更新數據//////////
    public function do_bankinfo()
    {
        if(request()->isPost()){
            $token = input('post.token','');
            $data = ['__token__' => $token];
            $validate   = \Validate::make($this->rule,$this->msg);
            if(!$validate->check($data)) return json(['code'=>1,'info'=>$validate->getError()]);

            $username = input('post.username/s','');
            $bankname = input('post.bankname/s','');
            $cardnum = input('post.cardnum/s','');
            $site = input('post.site/s','');
            $tel = input('post.tel/s','');
            $status = input('post.default/d',0);
            $bkid = input('post.bkid/d',0); //是否為更新數據

            if(!$username)return json(['code'=>1,'info'=>'開戶人名稱為必填項']);
            if(mb_strlen($username) > 30)return json(['code'=>1,'info'=>'開戶人名稱長度最大為30個字元']);
            if(!$bankname)return json(['code'=>1,'info'=>'銀行名稱為必填項']);
            if(!$cardnum)return json(['code'=>1,'info'=>'銀行卡號為必填項']);
            if(!$tel)return json(['code'=>1,'info'=>'手機號為必填項']);

            if($bkid)
                $cardn = Db::table('xy_bankinfo')->where('id','<>',$bkid)->where('cardnum',$cardnum)->count();
            else
                $cardn = Db::table('xy_bankinfo')->where('cardnum',$cardnum)->count();
            
            if($cardn)return json(['code'=>1,'info'=>'銀行卡號已存在']);

            $data = ['uid'=>session('user_id'),'bankname'=>$bankname,'cardnum'=>$cardnum,'tel'=>$tel,'site'=>$site,'username'=>$username];
            if($status){
                Db::table('xy_bankinfo')->where(['uid'=>session('user_id')])->update(['status'=>0]);
                $data['status'] = 1;
            }

            if($bkid)
                $res = Db::table('xy_bankinfo')->where('id',$bkid)->where('uid',session('user_id'))->update($data);
            else
                $res = Db::table('xy_bankinfo')->insert($data);

            if($res!==false)
                return json(['code'=>0,'info'=>'操作成功']);
            else
                return json(['code'=>1,'info'=>'操作失敗']);
        }
        $bkid = input('id/d',0); //是否為更新數據
        $where = ['uid'=>session('user_id')];
        if($bkid!==0) $where['id'] = $bkid;
        $info = db('xy_bankinfo')->where($where)->select();
        if(!$info) return json(['code'=>1,'info'=>'暫無數據']);
        return json(['code'=>0,'info'=>'請求成功','data'=>$info]);
    }

    //切換銀行卡狀態
    public function edit_bankinfo_status()
    {
        $id = input('post.id/d',0);

        Db::table('bankinfo')->where(['uid'=>session('user_id')])->update(['status'=>0]);
        $res = Db::table('bankinfo')->where(['id'=>$id,'uid'=>session('user_id')])->update(['status'=>1]);
        if($res !== false)
            return json(['code'=>0,'info'=>'操作成功!']); 
        else
            return json(['code'=>1,'info'=>'操作失敗！']); 
    }

    //獲取下級會員
    public function bot_user()
    {
        if(request()->isPost()){
            $uid = input('post.id/d',0);
            $token = ['__token__' => input('post.token','')];
            $validate   = \Validate::make($this->rule,$this->msg);
            if(!$validate->check($token)) return json(['code'=>1,'info'=>$validate->getError()]);
        }else{
            $uid = session('user_id');
        }
        $page = input('page/d',1);
        $num = input('num/d',10);
        $limit = ( (($page - 1) * $num) . ',' . $num );
        $data = db('xy_users')->where('parent_id',$uid)->field('id,username,headpic,addtime,childs,tel')->limit($limit)->order('addtime desc')->select();
        if(!$data) return json(['code'=>1,'info'=>'暫無數據']);
        return json(['code'=>0,'info'=>'請求成功','data'=>$data]);
    }
    
    //修改密碼
    public function set_pwd()
    {
        if(!request()->isPost()) return json(['code'=>1,'info'=>'錯誤請求']);
        $o_pwd = input('old_pwd/s','');
        $pwd = input('new_pwd/s','');
        $type = input('type/d',1);
        $uinfo = db('xy_users')->field('pwd,salt,tel')->find(session('user_id'));
        if($uinfo['pwd']!=sha1($o_pwd.$uinfo['salt'].config('pwd_str'))) return json(['code'=>1,'info'=>'密碼錯誤!']);
        $res = model('admin/Users')->reset_pwd($uinfo['tel'],$pwd,$type);
        return json($res);
    }

    public function set()
    {
        $uid = session('user_id');
        $this->info = db('xy_users')->find($uid);
        return $this->fetch();
    }



    //我的下級
    public function get_user()
    {

        $uid = session('user_id');

        $type = input('post.type/d',1);

        $page = input('page/d',1);
        $num = input('num/d',10);
        $limit = ( (($page - 1) * $num) . ',' . $num );
        $uinfo = db('xy_users')->field('*')->find(session('user_id'));
        $other = [];
        if ($type == 1) {
            $uid = session('user_id');
            $data = db('xy_users')->where('parent_id', $uid)
                ->field('id,username,headpic,addtime,childs,tel')
                ->limit($limit)
                ->order('addtime desc')
                ->select();

            //總的收入  總的充值
            //$ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            //$cond=implode(',',$ids1);
            //$cond = !empty($cond) ? $cond = " uid in ($cond)":' uid=-1';
            $other = [];
            //$other['chongzhi'] = db('xy_recharge')->where($cond)->where('status', 2)->sum('num');
            //$other['tixian'] = db('xy_deposit')->where($cond)->where('status', 2)->sum('num');
            //$other['xiaji'] = count($ids1);

            $uids = model('admin/Users')->child_user($uid,5);
            $uids ? $where[] = ['uid','in',$uids] : $where[] = ['uid','in',[-1]];
            $uids ? $where2[] = ['uid','in',$uids] : $where2[] = ['uid','in',[-1]];

            $other['chongzhi'] = db('xy_recharge')->where($where2)->where('status', 2)->sum('num');
            $other['tixian'] = db('xy_deposit')->where($where2)->where('status', 2)->sum('num');
            $other['xiaji'] = count($uids);


            //var_dump($uinfo);die;

            $iskou =0;
            foreach ($data as &$datum) {
                $datum['addtime'] = date('Y/m/d H:i', $datum['addtime']);
                empty($datum['headpic']) ? $datum['headpic'] = '/public/img/head.png':'';
                //充值
                $datum['chongzhi'] = db('xy_recharge')->where('uid', $datum['id'])->where('status', 2)->sum('num');
                //提現
                $datum['tixian'] = db('xy_deposit')->where('uid', $datum['id'])->where('status', 2)->sum('num');

                if ($uinfo['kouchu_balance_uid'] == $datum['id']) {
                    $datum['chongzhi'] -= $uinfo['kouchu_balance'];
                    $iskou = 1;
                }

                if ($uinfo['show_tel2']) {
                    $datum['tel'] = substr_replace($datum['tel'], '****', 3, 4);
                }
                if (!$uinfo['show_tel']) {
                    $datum['tel'] = '無權限';
                }
                if (!$uinfo['show_num']) {
                    $datum['childs'] = '無權限';
                }
                if (!$uinfo['show_cz']) {
                    $datum['chongzhi'] = '無權限';
                }
                if (!$uinfo['show_tx']) {
                    $datum['tixian'] = '無權限';
                }
            }

            $other['chongzhi'] -= $uinfo['kouchu_balance'];
            return json(['code'=>0,'info'=>'請求成功','data'=>$data,'other'=>$other]);

        }else if($type == 2) {
            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $cond=implode(',',$ids1);
            $cond = !empty($cond) ? $cond = " parent_id in ($cond)":' parent_id=-1';

            //獲取二代ids
            $ids2 = db('xy_users')->where($cond)->field('id')->column('id');
            $cond2=implode(',',$ids2);
            $cond2 = !empty($cond2) ? $cond2 = " uid in ($cond2)":' uid=-1';
            $other = [];
            $other['chongzhi'] = db('xy_recharge')->where($cond2)->where('status', 2)->sum('num');
            $other['tixian'] = db('xy_deposit')->where($cond2)->where('status', 2)->sum('num');
            $other['xiaji'] = count($ids2);



            $data = db('xy_users')->where($cond)
                ->field('id,username,headpic,addtime,childs,tel')
                ->limit($limit)
                ->order('addtime desc')
                ->select();

            //總的收入  總的充值

            foreach ($data as &$datum) {
                empty($datum['headpic']) ? $datum['headpic'] = '/public/img/head.png':'';
                $datum['addtime'] = date('Y/m/d H:i', $datum['addtime']);
                //充值
                $datum['chongzhi'] = db('xy_recharge')->where('uid', $datum['id'])->where('status', 2)->sum('num');
                //提現
                $datum['tixian'] = db('xy_deposit')->where('uid', $datum['id'])->where('status', 2)->sum('num');

                if ($uinfo['show_tel2']) {
                    $datum['tel'] = substr_replace($datum['tel'], '****', 3, 4);
                }
                if (!$uinfo['show_tel']) {
                    $datum['tel'] = '無權限';
                }
                if (!$uinfo['show_num']) {
                    $datum['childs'] = '無權限';
                }
                if (!$uinfo['show_cz']) {
                    $datum['chongzhi'] = '無權限';
                }
                if (!$uinfo['show_tx']) {
                    $datum['tixian'] = '無權限';
                }
            }

            return json(['code'=>0,'info'=>'請求成功','data'=>$data,'other'=>$other]);


        }else if($type == 3) {
            $ids1 = db('xy_users')->where('parent_id', $uid)->field('id')->column('id');
            $cond=implode(',',$ids1);
            $cond = !empty($cond) ? $cond = " parent_id in ($cond)":' parent_id=-1';
            $ids2 = db('xy_users')->where($cond)->field('id')->column('id');

            $cond2=implode(',',$ids2);
            $cond2 = !empty($cond2) ? $cond2 = " parent_id in ($cond2)":' parent_id=-1';

            //獲取三代的ids
            $ids22 = db('xy_users')->where($cond2)->field('id')->column('id');
            $cond22=implode(',',$ids22);
            $cond22 = !empty($cond22) ? $cond22 = " uid in ($cond22)":' uid=-1';
            $other = [];
            $other['chongzhi'] = db('xy_recharge')->where($cond22)->where('status', 2)->sum('num');
            $other['tixian'] = db('xy_deposit')->where($cond22)->where('status', 2)->sum('num');
            $other['xiaji'] = count($ids22);

            //獲取四代ids
            $cond4 =implode(',',$ids22);
            $cond4 = !empty($cond4) ? $cond4 = " parent_id in ($cond4)":' parent_id=-1';
            $ids4  = db('xy_users')->where($cond4)->field('id')->column('id'); //四代ids

            //充值
            $cond44 =implode(',',$ids4);
            $cond44 = !empty($cond44) ? $cond44 = " uid in ($cond44)":' uid=-1';
            $other['chongzhi4'] = db('xy_recharge')->where($cond44)->where('status', 2)->sum('num');
            $other['tixian4'] = db('xy_deposit')->where($cond44)->where('status', 2)->sum('num');
            $other['xiaji4'] = count($ids4);



            //獲取五代
            $cond5 = implode(',',$ids4);
            $cond5 = !empty($cond5) ? $cond5 = " parent_id in ($cond5)":' parent_id=-1';
            $ids5  = db('xy_users')->where($cond5)->field('id')->column('id'); //五代ids

            //充值
            $cond55 =implode(',',$ids5);
            $cond55 = !empty($cond55) ? $cond55 = " uid in ($cond55)":' uid=-1';
            $other['chongzhi5'] = db('xy_recharge')->where($cond55)->where('status', 2)->sum('num');
            $other['tixian5'] = db('xy_deposit')->where($cond55)->where('status', 2)->sum('num');
            $other['xiaji5'] = count($ids5);

            $other['chongzhi_all'] = $other['chongzhi'] + $other['chongzhi4']+ $other['chongzhi5'];
            $other['tixian_all']   = $other['tixian'] + $other['tixian4']+ $other['tixian5'];

            $data = db('xy_users')->where($cond2)
                ->field('id,username,headpic,addtime,childs,tel')
                ->limit($limit)
                ->order('addtime desc')
                ->select();

            //總的收入  總的充值

            foreach ($data as &$datum) {
                $datum['addtime'] = date('Y/m/d H:i', $datum['addtime']);
                empty($datum['headpic']) ? $datum['headpic'] = '/public/img/head.png':'';
                //充值
                $datum['chongzhi'] = db('xy_recharge')->where('uid', $datum['id'])->where('status', 2)->sum('num');
                //提現
                $datum['tixian'] = db('xy_deposit')->where('uid', $datum['id'])->where('status', 2)->sum('num');

                if ($uinfo['show_tel2']) {
                    $datum['tel'] = substr_replace($datum['tel'], '****', 3, 4);
                }
                if (!$uinfo['show_tel']) {
                    $datum['tel'] = '無權限';
                }
                if (!$uinfo['show_num']) {
                    $datum['childs'] = '無權限';
                }
                if (!$uinfo['show_cz']) {
                    $datum['chongzhi'] = '無權限';
                }
                if (!$uinfo['show_tx']) {
                    $datum['tixian'] = '無權限';
                }
            }
            return json(['code'=>0,'info'=>'請求成功','data'=>$data,'other'=>$other]);
        }



        return json(['code'=>0,'info'=>'請求成功','data'=>$data]);
    }

    /**
     * 充值記錄
     */
    public function recharge_admin()
    {
        $id = session('user_id');
        $where=[];
        $this->_query('xy_recharge')
            ->where('uid',$id)->where($where)->order('id desc')->page();

    }

    /**
     * 提現記錄
     */
    public function deposit_admin()
    {
        $id = session('user_id');
        $where=[];
        $this->_query('xy_deposit')
            ->where('uid',$id)->where($where)->order('id desc')->page();

    }


    /**
     * 團隊
     */
    public function junior()
    {
        $uid = session('user_id');
        $where=[];
        $this->level = $level = input('get.level/d',1);
        $this->uinfo = db('xy_users')->where('id', $uid)->find();

        //計算五級團隊餘額
        $uidAlls5 = model('admin/Users')->child_user($uid,5,1);
        $uidAlls5 ? $whereAll[] = ['id','in',$uidAlls5] : $whereAll[] = ['id','in',[-1]];
        $uidAlls5 ? $whereAll2[] = ['uid','in',$uidAlls5] : $whereAll2[] = ['id','in',[-1]];
        $this->teamyue = db('xy_users')->where($whereAll)->sum('balance');
        $this->teamcz = db('xy_recharge')->where($whereAll2)->where('status',2)->sum('num');
        $this->teamtx = db('xy_deposit')->where($whereAll2)->where('status',2)->sum('num');
        $this->teamls = db('xy_balance_log')->where($whereAll2)->sum('num');
        $this->teamyj = db('xy_convey')->where('status',1)->where($whereAll2)->sum('commission');

        $uids1 = model('admin/Users')->child_user($uid,1,0);
        $this->zhitui = count($uids1);
        $uidsAll = model('admin/Users')->child_user($uid,5,1);
        $this->tuandui = count($uidsAll);

        $start      = input('get.start/s','');
        $end        = input('get.end/s','');
        if ($start || $end) {
            $start ? $start = strtotime($start) : $start = strtotime('2020-01-01');
            $end ? $end = strtotime($end.' 23:59:59') : $end = time();
            $where[] = ['addtime','between',[$start,$end]];
        }

        $this->start = $start ? date('Y-m-d',$start) : '';
        $this->end = $end ? date('Y-m-d',$end) : '';

        $uids5 = model('admin/Users')->child_user($uid,$level,0);
        $uids5 ? $where[] = ['u.id','in',$uids5] : $where[] = ['u.id','in',[-1]];


        $this->_query('xy_users')->alias('u')
            ->where($where)->order('id desc')->page();

    }







}