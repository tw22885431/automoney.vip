<?php

namespace app\index\controller;

use library\Controller;
use think\facade\Request;
use think\db;

class Send extends Controller
{
    //獲取驗證碼
    public function sendsms(Request $request)
    {
        $tel = Request::post('tel/s','');
        $type = Request::post('type',1);
        if(!is_mobile($tel)){
            return json(['code'=>1,'info'=>'手機號碼格式不正確']);
        }

        if($type == 1){
            $num = Db::table('xy_users')->where(['tel'=>$tel])->count();
            if($num){
                return json(['code'=>1,'info'=>'手機號碼已注冊']);
            }
        }

        $res = Db::table('xy_verify_msg')->field('addtime,tel')->where(['tel'=>$tel])->find();
        if($res && (($res['addtime'] + 60) > time()))
            return json(['code'=>0,'info'=>'1分鐘內只能发送一條短信']);

        $time = date('YmdHis',time());
        $num = rand(10000,99999);
        //$msg = config('app.zhangjun_sms.content') . $num  . '，' . config('app.zhangjun_sms.min') . '分鐘內有效！';
        //$result = \org\ZhangjunSms::sendsms(config('app.zhangjun_sms.userid'),$time,md5(config('app.zhangjun_sms.account').config('app.zhangjun_sms.pwd').$time),$tel,$msg);

        $result = $this->smsbao($tel,$num);

        if($result['status'] == 1){  //发送成功
            if(!$res){
                $r = Db::table('xy_verify_msg')->insert(['tel'=>$tel, 'msg'=>$num, 'addtime'=>time(),'type'=>$type]);
            }else{
                $r = Db::table('xy_verify_msg')->where(['tel'=>$tel])->data(['msg'=>$num,'addtime'=>time(),'type'=>$type])->update();
            }

            if($r)
                return json(['code'=>0,'info'=>'发送成功']);
            else
                return json(['code'=>0,'info'=>'发送失敗']);
        }else
            return $result;
    }


    public function smsbao($tel,$code)
    {
        //----------------短信寶---------------------
        $statusStr = array(
            "0" => "短信发送成功",
            "-1" => "參數不全",
            "-2" => "服務器空間不支持,請確認支持curl或者fsocket，聯系您的空間商解決或者更換空間！",
            "30" => "密碼錯誤",
            "40" => "賬號不存在",
            "41" => "余額不足",
            "42" => "帳戶已過期",
            "43" => "IP地址限制",
            "50" => "內容含有敏感詞"
        );
        $smsapi = "http://api.smsbao.com/";
        $user = config('app.smsbao.user');       //短信平台帳號15196952584
        $pass = config('app.smsbao.pass') ;
        $pass = md5("$pass");   //短信平台密碼
        $sign = config('app.smsbao.sign') ;
        $content = "【".$sign."】您的驗證碼為{$code}，驗證碼5分鐘內有效。";
        $phone = $tel;//要发送短信的手機號碼
        $sendurl = $smsapi . "sms?u=" . $user . "&p=" . $pass . "&m=" . $phone . "&c=" . urlencode($content);
        $result = file_get_contents($sendurl);

        if ($result == '0') {
            return ['status' => 1, 'msg' => "發送成功"];
        } else {
            return ['status' => 0, 'msg' => $statusStr[$result]];
        }

    }

}
