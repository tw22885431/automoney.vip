<?php
namespace app\index\controller;

use library\Controller;
use think\facade\Request;
use think\Db;

/**
 * 驗證登錄控制器
 */
class Base extends Controller
{
    protected $rule = ['__token__' => 'token'];
    protected $msg  = ['__token__'  => '無效token！'];

    function __construct() {
        parent::__construct();
        $uid = session('user_id');
        if (!$uid) {
            $uid = cookie('user_id');
        }

        if(!$uid && request()->isPost()){
            $this->error('請先登錄');
        }
        if(!$uid) $this->redirect('User/login');
        $this->console = db('xy_script')->where('id',1)->value('script');
    }

    /**
     * 空操作 用於顯示錯誤頁面
     */
    public function _empty($name){
        return $this->fetch($name);
    }

    //圖片上傳為base64為的圖片
    public function upload_base64($type,$img){
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $img, $result)){
            $type_img = $result[2];  //得到圖片的後綴
            //上傳 的文件目錄

            $App = new \think\App();
            $new_files = $App->getRootPath() . 'upload'. DIRECTORY_SEPARATOR . $type. DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m-d') . DIRECTORY_SEPARATOR ;

            if(!file_exists($new_files)) {
                //檢查是否有該文件夾，如果沒有就創建，並給予最高權限
                //服務器給文件夾權限
                mkdir($new_files, 0777,true);
            }
            $new_files = check_pic($new_files,".{$type_img}");
            if (file_put_contents($new_files, base64_decode(str_replace($result[1], '', $img)))){
                //上傳成功後  得到信息
                $filenames=str_replace('\\', '/', $new_files);
                $file_name=substr($filenames,strripos($filenames,"/upload"));
                return $file_name;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * 檢查交易狀態
     */
    public function check_deal()
    {
        $uid = session('user_id');
        $uinfo = db('xy_users')->field('deal_status,status,balance,level,deal_count,deal_time,deal_reward_count dc')->find($uid);
        if($uinfo['status']==2) return ['code'=>1,'info'=>'該賬戶已被禁用'];
        if($uinfo['deal_status']==0) return ['code'=>1,'info'=>'該賬戶交易功能已被凍結'];
        if($uinfo['deal_status']==3) return ['code'=>1,'info'=>'該賬戶存在未完成訂單，無法繼續搶單！'];
        if($uinfo['balance']<config('deal_min_balance')) return ['code'=>1,'info'=>'余額低於'.config('deal_min_balance').'，無法繼續交易'];
        if($uinfo['deal_time']==strtotime(date('Y-m-d'))){
            //交易次數限制
            $level = $uinfo['level'];
            !$uinfo['level'] ? $level = 0 : '';
            $ulevel = Db::name('xy_level')->where('level',$level)->find();
            if ($uinfo['deal_count'] >= $ulevel['order_num']) {
                return ['code'=>1,'info'=>'會員等級交易次數不足!'];
            }
        }else{
            //重置最後交易時間
            db('xy_users')->where('id',$uid)->update(['deal_time'=>strtotime(date('Y-m-d')),'deal_count'=>0,'recharge_num'=>0,'deposit_num'=>0]);
        }

        return false;
    }

}
