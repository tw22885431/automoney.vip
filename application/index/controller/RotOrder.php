<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;

/**
 * 下單控制器
 */
class RotOrder extends Base
{
    /**
     * 首頁
     */
    public function index()
    {
        $where = [
            ['uid','=',session('user_id')],
            ['addtime','between',strtotime(date('Y-m-d')).','.time()],
        ];
        $this->day_deal = Db::name('xy_convey')->where($where)->where('status','in',[1,3,5])->sum('commission');

        $yes1 = strtotime( date("Y-m-d 00:00:00",strtotime("-1 day")) );
        $yes2 = strtotime( date("Y-m-d 23:59:59",strtotime("-1 day")) );
        $this->price = Db::name('xy_users')->where('id',session('user_id'))->sum('balance');

        $this->day_d_count = Db::name('xy_convey')->where($where)->where('status','in',[0,1,3,5])->count('id');
        $freeze_balance = Db::name('xy_users')->where('id',session('user_id'))->sum('freeze_balance');
        $xy_deposit = Db::name('xy_deposit')->where('uid',session('user_id'))->where('type',1)->sum('num');
        $this->lock_deal = $freeze_balance+$xy_deposit;
        $this->yes_team_num = Db::name('xy_reward_log')->where('uid',session('user_id'))->where('addtime','between',[$yes1,$yes2])->where('status',1)->sum('num');//獲取下級返傭數額
        $this->today_team_num = Db::name('xy_reward_log')->where('uid',session('user_id'))->where('addtime','between',[strtotime('Y-m-d'),time()])->where('status',1)->sum('num');//獲取下級返傭數額

        //分類
        $type = input('get.type/d',1);
        $this->cate = Db::name('xy_goods_cate')->alias('c')
            ->leftJoin('xy_level u','u.id=c.level_id')
            ->field('c.name,c.cate_info,c.cate_pic,u.name as levelname,u.pic,u.level,u.bili,u.order_num')
            ->find($type);;
        $this->beizhu = db('xy_index_msg')->where('id',9)->value('content');;


        $this->yes_user_yongjin = db('xy_convey')->where('uid',session('user_id'))->where('status',1)->where('addtime','between',[$yes1,$yes2])->sum('commission');
        $this->user_yongjin = db('xy_convey')->where('uid',session('user_id'))->where('status',1)->sum('commission');


        return $this->fetch();
    }
  /**
    *提交搶單
    */
    public function submit_order()
    {
        $tmp = $this->check_deal();
        if($tmp) return json($tmp);
        $res = check_time(9,22);
        //if($res) return json(['code'=>1,'info'=>'禁止在9:00~22:00以外的時間段執行當前操作!']);

        $res = check_time(config('order_time_1'),config('order_time_2'));
        $str = config('order_time_1').":00  - ".config('order_time_2').":00";
        if($res) return json(['code'=>1,'info'=>'禁止在'.$str.'以外的時間段執行當前操作!']);
        
		$io1 = db('xy_io_log')->where('id',1)->find();
		$io2 = db('xy_io_log')->where('id',2)->find();
		$io3 = db('xy_io_log')->where('id',3)->find();
		$where = [
            ['addtime','between',strtotime(date('Y-m-d')).','.time()],
        ];
        $moneys = db('xy_convey')->where($where)->sum('num');
        $nowtime = time();
        $time1 = strtotime($io1['time']);
        $time2 = strtotime($io2['time']);
        $time3 = strtotime($io3['time']);
        $money1 = $io1['money'];
        $money2 = $io2['money']+$money1;
        $money3 = $io3['money']+$money1+$money2;
        $res = check_time(config('order_time_1'),config('order_time_2'));
        $str = config('order_time_1').":00  - ".config('order_time_2').":00";
        if($res) return json(['code'=>1,'info'=>'禁止在'.$str.'以外的時間段執行當前操作!']);
		
		//第一時段限制
        if($nowtime > $time1 && $nowtime < $time2 && $moneys >= $money1) return json(['code'=>1,'info'=>'本時段訂單已被搶完，請下壹時段再來！']);
        
        //第二時段限制
        if($nowtime > $time2 && $nowtime < $time3 && $moneys >= $money2) return json(['code'=>1,'info'=>'本時段訂單已被搶完，請下壹時段再來！']);
        
        //第三時段限制
        if($nowtime > $time3 && $moneys >= $money3) return json(['code'=>1,'info'=>'今日訂單已被搶完，請明天再來！']);
        $uid = session('user_id');
        $add_id = db('xy_member_address')->where('uid',$uid)->value('id');//獲取收款地址信息
        if(!$add_id) return json(['code'=>1,'info'=>'還沒有設置收貨地址']);
        //檢查交易狀態
        // $sleep = mt_rand(config('min_time'),config('max_time'));
        $res = db('xy_users')->where('id',$uid)->update(['deal_status'=>2]);//將賬戶狀態改為等待交易
        if($res === false) return json(['code'=>1,'info'=>'搶單失敗，請稍後再試！']);
        // session_write_close();//解決sleep造成的進程阻塞問題
        // sleep($sleep);
        //
        $cid = input('post.cid/d',1);
        $count = db('xy_goods_list')->where('cid','=',$cid)->count();
        

        if($count < 1) return json(['code'=>1,'info'=>'搶單失敗，商品庫存不足！']);


        $res = model('admin/Convey')->create_order($uid,$cid);
        return json($res);
    }

    /**
     * 停止搶單
     */
    public function stop_submit_order()
    {
        $uid = session('user_id');
        $res = db('xy_users')->where('id',$uid)->where('deal_status',2)->update(['deal_status'=>1]);
        if($res){
            return json(['code'=>0,'info'=>'操作成功!']);
        }else{
            return json(['code'=>1,'info'=>'操作失敗!']);
        }
    }

}