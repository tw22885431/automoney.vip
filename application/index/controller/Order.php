<?php

namespace app\index\controller;

use think\Controller;
use think\Db;
use think\Request;

/**
 * 訂單列表
 */
class Order extends Base
{


    public function index()
    {
        $this->status = $status= input('get.status/d',0);
        $where =[];
        if ($status) {
            $status == -1 ? $status = 0:'';
            $where['xc.status'] = $status;
        }
        $uid = session('user_id');
        $this->balance = Db::name('xy_users')->where('id',$uid)->value('balance');//獲取用戶今日已充值金額


        $this->_query('xy_convey')
            ->where('xc.uid',session('user_id'))
            ->alias('xc')
            ->leftJoin('xy_goods_list xg','xc.goods_id=xg.id')
            ->field('xc.*,xg.goods_name,xg.shop_name,xg.goods_price,xg.goods_pic')
            ->order('xc.addtime desc')
            ->where($where)
            ->page();
        return $this->fetch();
    }








    /**
     * 獲取訂單列表
     */
    public function order_list()
    {
        $page = input('post.page/d',1);
        $num = input('post.num/d',10);
        $limit = ( (($page - 1) * $num) . ',' . $num );
        $type = input('post.type/d',1);
        switch($type){
            case 1: //獲取待處理訂單
                $type = 0;
                break;
            case 2: //獲取凍結中訂單
                $type = 5;
                break;
            case 3: //獲取已完成訂單
                $type = 1;
                break;
        }
        $data = db('xy_convey')
                ->where('xc.uid',session('user_id'))
                ->where('xc.status',$type)
                ->alias('xc')
                ->leftJoin('xy_goods_list xg','xc.goods_id=xg.id')
                ->field('xc.*,xg.goods_name,xg.shop_name,xg.goods_price,xg.goods_pic')
                ->order('xc.addtime desc')
                ->limit($limit)
                ->select();
        
        foreach ($data as &$datum) {
            $datum['endtime'] = date('Y/m/d H:i:s',$datum['endtime']);
            $datum['addtime'] = date('Y/m/d H:i:s',$datum['addtime']);
        }


        if(!$data) json(['code'=>1,'info'=>'暫無數據']);
        return json(['code'=>0,'info'=>'請求成功','data'=>$data]);
    }

    /**
     * 獲取單筆訂單詳情
     */
    public function order_info()
    {
        if(\request()->isPost()){
            $oid = input('post.id','');
            $oinfo = db('xy_convey')
                        ->alias('xc')
                        ->leftJoin('xy_member_address ar','ar.uid=xc.uid','ar.is_default=1')
                        ->leftJoin('xy_goods_list xg','xg.id=xc.goods_id')
                        ->leftJoin('xy_users u','u.id=xc.uid')
                        ->field('xc.id oid,xc.commission,xc.addtime,xc.endtime,xc.status,xc.num,xc.goods_count,xc.add_id,xg.goods_name,xg.goods_price,xg.shop_name,xg.goods_pic,ar.name,ar.tel,ar.address,u.balance')
                        ->where('xc.id',$oid)
                        ->where('xc.uid',session('user_id'))
                        ->find();
            if(!$oinfo) return json(['code'=>1,'暫無數據']);
            $oinfo['endtime'] = date('Y/m/d H:i:s', $oinfo['endtime']  );
            $oinfo['addtime'] = date('Y/m/d H:i:s', $oinfo['addtime']  );

            return json(['code'=>0,'info'=>'請求成功','data'=>$oinfo]);
        }
    }
    
    /**
     * 處理訂單
     */
    public function do_order()
    {
        if(request()->isPost()){
            $oid = input('post.oid/s','');
            $status = input('post.status/d',1);
            $add_id = input('post.add_id/d',0);
            if(!\in_array($status,[1,2])) json(['code'=>1,'info'=>'參數錯誤']);

            $res = model('admin/Convey')->do_order($oid,$status,session('user_id'),$add_id);
            return json($res);
        }
        return json(['code'=>1,'info'=>'錯誤請求']);
    }

    /**
     * 獲取充值訂單
     */
    public function get_recharge_order()
    {
        $uid = session('user_id');
        $page = input('post.page/d',1);
        $num = input('post.num/d',10);
        $limit = ( (($page - 1) * $num) . ',' . $num );
        $info = db('xy_recharge')->where('uid',$uid)->order('addtime desc')->limit($limit)->select();
        if(!$info) return json(['code'=>1,'info'=>'暫無數據']);
        return json(['code'=>0,'info'=>'請求成功','data'=>$info]);
    }

    /**
     * 驗證提現密碼
     */
    public function check_pwd2()
    {
        if(!request()->isPost()) return json(['code'=>1,'info'=>'錯誤請求']);
        $pwd2 = input('post.pwd2/s','');
        $info = db('xy_users')->field('pwd2,salt2')->find(session('user_id'));
        if($info['pwd2']=='') return json(['code'=>1,'info'=>'未設置交易密碼']);
        if($info['pwd2']!=sha1($pwd2.$info['salt2'].config('pwd_str'))) return json(['code'=>1,'info'=>'密碼錯誤']);
        return json(['code'=>0,'info'=>'驗證通過']);
    }
}