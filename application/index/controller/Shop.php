<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版權所有 2014~2019 
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 

// +----------------------------------------------------------------------

namespace app\index\controller;

use library\Controller;
use think\Db;

/**
 * 商城
 * Class Index
 * @package app\index\controller
 */
class Shop extends Base
{
    /**
     * 入口跳轉鏈接
     */
    public function index2()
    {
        $this->redirect('home');
    }

    public function index()
    {
        $this->banner = Db::name('xy_banner')->select();
        $this->gundong = db('xy_index_msg')->where('id',8)->value('content');;;
        $this->shoplist = db('xy_shop_goods_list')->where('is_tj',1)->limit(10)->select();;



        $this->assign('pic','/upload/qrcode/user/'.(session('user_id')%20).'/'.session('user_id').'-1.png');
        $this->cate = db('xy_shop_goods_cate')->order('addtime desc')->select();

        //一天的
        $this->lixibao = db('xy_lixibao_list')->order('id asc')->find();

        return $this->fetch();
    }


    public function goodslist()
    {
        $where = [];
        if(input('cid/d',0))$where[] = ['cid','=',input('cid/d',0)];
        if(input('name/s',''))$where[] = ['goods_name','like','%' . input('name/s','') . '%'];

        //一天的

        $this->_query('xy_shop_goods_list')->where($where)->page();
    }


    public function orderlist()
    {
        $where = [];
        if(input('cid/d',0))$where[] = ['cid','=',input('cid/d',0)];
        if(input('name/s',''))$where[] = ['goods_name','like','%' . input('name/s','') . '%'];

        //一天的
        if(request()->isPost()) {
            $uid = session('user_id');
            $page = input('post.page/d',1);
            $num = input('post.num/d',10);
            $limit = ( (($page - 1) * $num) . ',' . $num );
            $status = input('post.status/d',1);

            $data = db('xy_shop_order')
                ->where('uid',session('user_id'))
                ->where('status',$status)
                ->order('addtime desc')
                ->limit($limit)
                ->select();
            foreach ($data as &$datum) {
                $datum['addtime'] = date('Y/m/d H:i:s',$datum['addtime']);
            }
            if(!$data) json(['code'=>1,'info'=>'暫無數據']);
            return json(['code'=>0,'info'=>'請求成功','data'=>$data]);
        }
        return $this->fetch();
    }


    public function detail()
    {
        $id      = input('get.id/d',1);
        $this->info = db('xy_shop_goods_list')->find($id);;

        return $this->fetch();
    }

   public function order_info()
    {
        $uid = session('user_id');
        $id      = input('get.id/d',1);
        $this->endtime = date('Y/m/d H:i:s', time()+30*60);
        $this->address = db('xy_member_address')->where('uid',$uid)->find();
        $recharge = db('xy_recharge')->where('uid',$uid)->where('status',2)->count();
        if($recharge < 1){
        	$this->balance = '0.00';
        }else{
        	$this->balance = db('xy_users')->where('id',$uid)->value('balance');
        }
        $this->jing_balance = db('xy_users')->where('id',$uid)->value('jing_balance');
        $this->dong_balance = db('xy_users')->where('id',$uid)->value('dong_balance');
        $this->goods = db('xy_shop_goods_list')->find($id);

        return $this->fetch();
    }



     public function order_detail()
    {
        $uid = session('user_id');
        $id      = input('get.oid/s',1);
        $this->endtime = date('Y/m/d H:i:s', time()+7*24*60*60);
        $this->address = db('xy_member_address')->where('uid',$uid)->find();
        $this->balance = db('xy_users')->where('id',$uid)->value('balance');

        $order = db('xy_shop_order')->where('id',$id)->find();

        $this->goods = db('xy_shop_goods_list')->find($order['gid']);
        $this->order = $order;


        return $this->fetch();
    }


    //生成訂單號
    function getSn($head='')
    {
        @date_default_timezone_set("PRC");
        $order_id_main = date('YmdHis') . mt_rand(1000, 9999);
        //唯一訂單號碼（YYMMDDHHIISSNNN）
        $osn = $head.substr($order_id_main,2); //生成訂單號
        return $osn;
    }
    
    
	//購買商品
    public function do_order()
    {
        $id = input('post.id','');
        $uid = session('user_id');
        $num = input('post.num',1);
        $maijia = input('post.maijia','');
        $goods = db('xy_shop_goods_list')->find($id);
        if (!$goods) {
            return json(['code'=>1,'info'=>'商品參數異常','data'=>[]]);
        }

        if ($num <= 0) {
            return json(['code'=>1,'info'=>'最低提交1個訂單','data'=>[]]);
        }
        if (!$num) return json(['code'=>1,'info'=>'參數異常','data'=>[]]);
        $recharge = Db::name('xy_recharge')->where('uid',$uid)->where('status',2)->count();
        $wallet_type = input('post.wallet_type','');//錢包選擇
        if($wallet_type == 1){
        	$balance = Db::name('xy_users')->where('id',$uid)->value('jing_balance');
        }elseif ($wallet_type == 2) {
        	$balance = Db::name('xy_users')->where('id',$uid)->value('dong_balance');
        }elseif ($wallet_type == 3 &&  $recharge < 1){
        	$balance = 0;
        }else{
        	$balance = Db::name('xy_users')->where('id',$uid)->value('balance');
        }
        if ( $balance < ($goods['goods_price']*$num) ) {
            return json(['code'=>1,'info'=>'可用余額不足,請先充值','data'=>[]]);
        }

        if( $goods['goods_price']*$num < 0 ) {
            return json(['code'=>1,'info'=>'you are sb','data'=>[]]);
        }

        $id1 = getSn('SP');
        $data = [
            'id'        =>$id1,
            'uid'       => $uid,
            'gid'       => $id,
            'price'       => $goods['goods_price'],
            'num'      => $num,
            'price2'      => $goods['goods_price']*$num,
            'status'   => 1,
            'paytype'  => $wallet_type,
            'maijia'   =>$maijia,
            'addtime'   => time()
        ];
        if($wallet_type == 1){
        	$res = Db::name('xy_users')->where('id',$uid)->setDec('jing_balance',$goods['goods_price']*$num);
        }elseif($wallet_type == 2){
        	$res = Db::name('xy_users')->where('id',$uid)->setDec('dong_balance',$goods['goods_price']*$num);
        }else{
        	$res = Db::name('xy_users')->where('id',$uid)->setDec('balance',$goods['goods_price']*$num);
        }

        //
        $res1 = Db::name('xy_balance_log')->insert([
            //記錄返傭信息
            'uid'       => $uid,
            'oid'       => $id1,
            'num'       => $goods['goods_price']*$num,
            'type'      => 11,
            'addtime'   => time()
        ]);

        $res = db('xy_shop_order')->insert($data);
        if($res)
            return json(['code'=>0,'info'=>'操作成功']);
        else
            return json(['code'=>1,'info'=>'操作失敗']);

    }
    
    
    
    //確定收貨商品
    public function do_shouhuo()
    {
        $id = input('post.id','');
        $uid = session('user_id');
        $goods = db('xy_shop_order')->find($id);
        if (!$goods) {
            return json(['code'=>1,'info'=>'訂單參數異常','data'=>[]]);
        }
        if ($goods['status'] ==2 ) {
            return json(['code'=>1,'info'=>'訂單已完成，請勿重覆提交！','data'=>[]]);
        }
        $data['status'] = 2;
        $res = Db::name('xy_shop_order')->where('id',$id)->update($data);
        if($res)
            return json(['code'=>0,'info'=>'操作成功']);
        else
            return json(['code'=>1,'info'=>'操作失敗']);

    }




}
