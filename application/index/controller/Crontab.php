<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版權所有 2014~2019 
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 

// +----------------------------------------------------------------------

namespace app\index\controller;

use library\Controller;
use think\Db;

/**
 * 定時器
 */
class Crontab extends Controller
{
    //凍結訂單
    public function freeze_order()
    {
        $timeout = time()-config('deal_timeout');//超時訂單
        $oinfo = Db::name('xy_convey')->where('status',0)->where('addtime','<=',$timeout)->field('id')->select();
        if($oinfo){
            foreach ($oinfo as $v) {
                Db::name('xy_convey')->where('id',$v['id'])->update(['status'=>5,'endtime'=>time()]);
            }
        }
        $this->cancel_order();
        $this->reset_deal();
    }

    //強制取消訂單並凍結賬戶 
    public function cancel_order()
    {
        $timeout = time()-config('deal_timeout');//超時訂單
        //$oinfo = Db::name('xy_convey')->field('id oid,uid')->where('status',5)->where('endtime','<=',$timeout)->select();
        $oinfo = Db::name('xy_convey')->field('id oid,uid')->where('status',0)->where('endtime','<=',$timeout)->select();
        if($oinfo){
            foreach ($oinfo as $v) {
                Db::name('xy_convey')->where('id',$v['oid'])->update(['status'=>4,'endtime'=>time()]);
                $tmp =Db::name('xy_users')->field('deal_error,deal_status')->find($v['uid']);
                //記錄違規信息
                if($tmp['deal_status']!=0){
                    if($tmp['deal_error'] < (int)config('deal_error')){
                        Db::name('xy_users')->where('id',$v['uid'])->update(['deal_status'=>1,'deal_error'=>Db::raw('deal_error+1')]);
                        Db::name('xy_user_error')->insert(['uid'=>$v['uid'],'oid'=>$v['oid'],'addtime'=>time(),'type'=>2]);
                    }elseif ($tmp['deal_error'] >= (int)config('deal_error')) {
                        Db::name('xy_users')->where('id',$v['uid'])->update(['deal_status'=>1,'deal_error'=>0]);
                        Db::name('xy_user_error')->insert(['uid'=>$v['uid'],'oid'=>$v['oid'],'addtime'=>time(),'type'=>3]);
                        //記錄交易凍結信息
                    }
                }
            }
        }
    }

    //解凍賬號
    public function reset_deal()
    {
        $uinfo = Db::name('xy_users')->where('deal_status',0)->field('id')->select();
        if($uinfo){
            foreach ($uinfo as $v) {
                $time = Db::name('xy_user_error')->where('uid',$v['id'])->where('type',3)->order('addtime desc')->limit(1)->value('addtime');
                if($time || $time <= time()-config('deal_feedze')){ 
                    //解封賬號
                    Db::name('xy_users')->where('id',$v['id'])->update(['deal_status'=>1]);
                    Db::name('xy_user_error')->insert(['uid'=>$v['id'],'oid'=>'-','addtime'=>time(),'type'=>1]);
                }
            }
        }
    }

    //发放傭金
    public function do_reward()
    {
        try {
            $time = strtotime(date('Y-m-d', time()));//獲取當天淩晨0點的時間戳
            $data = Db::name('xy_reward_log')->where('addtime','between', time()-3600*24 . ',' . time() )->where('status',1)->select();//獲取當天傭金
            if($data){
                foreach ($data as $k => $v) {
                    Db::name('xy_users')->where('id',$v['uid'])->setInc('balance',$v['num']);
                    Db::name('xy_reward_log')->where('id',$v['id'])->update(['status'=>2,'endtime'=>time()]);
                }
            }
            echo 1;
        } catch (\Throwable $th) {
            //throw $th;
        }
    }


    //定時器 解除凍結 反還傭金和本金
    public function start333()
    {
        $oinfo = Db::name('xy_convey')->where('status',5)->where('endtime','<=',time())->select();
        if ($oinfo) {
            //
            foreach ($oinfo as $v) {
                //
                Db::name('xy_convey')->where('id',$v['id'])->update(['status'=>1]);

                //
                $res1 = Db::name('xy_users')
                    ->where('id', $v['uid'])
                    //->dec('balance',$info['num'])//
                    ->inc('balance',$v['num']+$v['commission'])
                    //->inc('freeze_balance',$info['num']+$info['commission']) //凍結商品金額 + 傭金//
                    ->dec('freeze_balance',$v['num']+$v['commission']) //凍結商品金額 + 傭金
                    ->update(['deal_status'=>1]);
                $this->deal_reward($v['uid'],$v['id'],$v['num'],$v['commission']);

                //
            }
        }
        $this->cancel_order();
        $this->reset_deal();
        //$this->lixibao_chu();
        //var_dump($oinfo,time(),date('Y-m-d H:i:s', 1577812622));die;
        return json(['code'=>1,'info'=>'執行成功！']);
    }



    //------------------------------------------------------------------------------

    //強制取消訂單並凍結賬戶
    public function start() {
        $timeout = time()-config('deal_timeout');//超時訂單
        $timeout = time();//超時訂單
        //$oinfo = Db::name('xy_convey')->field('id oid,uid')->where('status',5)->where('endtime','<=',$timeout)->select();
        $oinfo = Db::name('xy_convey')->where('status',0)->where('endtime','<=',$timeout)->select();
        if($oinfo){
            $djsc = config('deal_feedze'); //凍結時長 單位小時
            foreach ($oinfo as $v) {
                Db::name('xy_convey')->where('id',$v['id'])->update(['status'=>5,'endtime'=>time()+ $djsc * 60 *60]);
                //$res = Db::name('xy_convey')->where('id',$oid)->update($tmp);
                $res1 = Db::name('xy_users')
                    ->where('id', $v['uid'])
                    ->dec('balance',$v['num'])
                    ->inc('freeze_balance',$v['num']+$v['commission']) //凍結商品金額 + 傭金
                    ->update(['deal_status' => 1,'status'=>1]);

                $res2 = Db::name('xy_balance_log')->insert([
                    'uid'           => $v['uid'],
                    'oid'           => $v['id'],
                    'num'           => $v['num'],
                    'type'          => 2,
                    'status'        => 2,
                    'addtime'       => time()
                ]);
            }
        }

        //解凍
        $this->jiedong();
    }

    public function jiedong()
    {
        $oinfo = Db::name('xy_convey')->where('status',5)->where('endtime','<=',time())->select();
        if ($oinfo) {
            //
            foreach ($oinfo as $v) {
                //
                Db::name('xy_convey')->where('id',$v['id'])->update(['status'=>1]);

                //
                $res1 = Db::name('xy_users')
                    ->where('id', $v['uid'])
                    //->dec('balance',$info['num'])//
                    ->inc('balance',$v['num']+$v['commission'])
                    //->inc('freeze_balance',$info['num']+$info['commission']) //凍結商品金額 + 傭金//
                    ->dec('freeze_balance',$v['num']+$v['commission']) //凍結商品金額 + 傭金
                    ->update(['deal_status'=>1]);
                $this->deal_reward($v['uid'],$v['id'],$v['num'],$v['commission']);

                //
            }
        }
    }


    /**
     * 交易返傭
     *
     * @return void
     */
    public function deal_reward($uid,$oid,$num,$cnum)
    {
        ///$res = Db::name('xy_users')->where('id',$uid)->where('status',1)->setInc('balance',$num+$cnum);

//        $res1 = Db::name('xy_balance_log')->insert([
//            //記錄返傭信息
//            'uid'       => $uid,
//            'oid'       => $oid,
//            'num'       => $num+$cnum,
//            'type'      => 3,
//            'addtime'   => time()
//        ]);
        Db::name('xy_balance_log')->where('oid',$oid)->update(['status'=>1]);


        //將訂單狀態改為已返回傭金
        Db::name('xy_convey')->where('id',$oid)->update(['c_status'=>1]);
        Db::name('xy_reward_log')->insert(['oid'=>$oid,'uid'=>$uid,'num'=>$num,'addtime'=>time(),'type'=>2]);//記錄充值返傭訂單
        /************* 发放交易獎勵 *********/
        $userList = model('admin/Users')->parent_user($uid,5);
        //echo '<pre>';
        //var_dump($userList);die;
        if($userList){
            foreach($userList as $v){
                if($v['status']===1){
                    Db::name('xy_reward_log')
                        ->insert([
                            'uid'       => $v['id'],
                            'sid'       => $v['pid'],
                            'oid'       => $oid,
                            'num'       => $num*config($v['lv'].'_d_reward'),
                            'lv'        => $v['lv'],
                            'type'      => 2,
                            'status'    => 1,
                            'addtime'   => time(),
                        ]);

                    //
                    $res1 = Db::name('xy_balance_log')->insert([
                        //記錄返傭信息
                        'uid'       => $v['id'],
                        'oid'       => $oid,
                        'sid'       => $uid,
                        'num'       => $cnum*config($v['lv'].'_d_reward'),
                        'type'      => 6,
                        'status'    => 1,
                        'f_lv'        => $v['lv'],
                        'addtime'   => time()
                    ]);

                }

                //
                $num3 = $num*config($v['lv'].'_d_reward'); //傭金
                $res = Db::name('xy_users')->where('id',$v['id'])->where('status',1)->setInc('balance',$num3);
                $res2 = Db::name('xy_balance_log')->insert([
                    'uid'           => $v['id'],
                    'oid'           => $oid,
                    'num'           => $num3,
                    'type'          => 6,
                    'status'        => 1,
                    'addtime'       => time()
                ]);

            }
        }
        /************* 发放交易獎勵 *********/

    }


   //----------------------------利息寶---------------------------------
    //1 轉入 2轉出  3每日收益
    public function lixibao_chu()
    {
        //處理從余額里轉出的到賬時間
        $addMax = time() - ( (config('lxb_time')) * 60*60 ); //向前退一個小時
        $res = Db::name('xy_lixibao')->where('status',0)->where('addtime','<=',$addMax)->where('type',2)->select();
        if ($res) {
            foreach ($res as $re) {
                $uid = $re['uid'];
                $num = $re['num'];

                Db::name('xy_users')->where('id',$re['id'])->setDec('lixibao_dj_balance',$num);  //利息寶月 -
                Db::name('xy_users')->where('id',$uid)->setInc('balance',$num);  //余額 +
                Db::name('xy_lixibao')->where('id',$re['id'])->update(['status'=>1]);  //利息寶月 -
            }
        }
    }

    public function lxb_jiesuan()
    {
        $now = time();
        $now = strtotime( date('Y-m-d 00:00:00', time()) );; //小於今天的 12點
        $lxb = Db::name('xy_lixibao')->where('endtime','<',$now)
            ->where('is_qu',0)
            ->where('is_sy',0)
            ->where('type',1)->select();  //利息寶月
        
        if ($lxb) {
            foreach ($lxb as $item) {
                //----------------------------------
                $lixibao = Db::name('xy_lixibao_list')->find($item['sid']);
                $price = $item['num'];
                $uid   = $item['uid'];
                $id    = $item['id'];
                $sy = $price * $lixibao['bili'] * $lixibao['day'];

                Db::name('xy_users')->where('id',$uid)->setDec('lixibao_balance',$price);  //利息寶余額 -
                Db::name('xy_users')->where('id',$uid)->setInc('balance',$price+$sy);  //余額 +  沒有手續費

                $res = Db::name('xy_lixibao')->where('id',$id)->update([
                    'is_qu'      => 1,
                    'is_sy'      => 1,
                    'real_num'     => $sy
                ]);
                $res1 = Db::name('xy_balance_log')->insert([
                    //記錄返傭信息
                    'uid'       => $uid,
                    'oid'       => $id,
                    'num'       => $sy,
                    'type'      => 23,
                    'addtime'   => time()
                ]);
                $res2 = Db::name('xy_balance_log')->insert([
                    //記錄返傭信息
                    'uid'       => $uid,
                    'oid'       => $id,
                    'num'       => $price,
                    'type'      => 22,
                    'addtime'   => time()
                ]);

                //自動結算 並自取出利息寶的 到用戶余額

                //----------------------------------
            }
        }
        return json(['code'=>1,'info'=>'執行成功！']);
    }



    /**
     * @地址      lixibao_js
     * @說明      每天12點 10分左右計算 前一天的收益  切莫重覆
     * @說明      域名為  http://域名/index/crontab/lixibao_js
     * @參數      @參數 @參數
     */
    //結算 //
    public function lixibao_js()
    {
        $uinfo = Db::name('xy_users')->where('lixibao_balance','>',0)->select();  //余額 +

        if ($uinfo) {
            foreach ($uinfo as $item) {
                $uid = $item['id'];
                //今日的購買不計算
                $yes1159 = strtotime( date("Y-m-d 23:59:59",strtotime("-1 day")) ); //昨天11:59
                //有效yue
                $yue = $item['lixibao_balance'];
                //59以後購買的e
                $after1159 = Db::name('xy_lixibao')->where('id',$item['id'])->where('addtime','>',$yes1159)->where('type',3)->sum('num');  //利息寶月 -
                !$after1159? $after1159 =0 :'';
                $yue = $yue - $after1159;

                //收益
                $shouyi = $yue * config('lxb_bili')*1;
                Db::name('xy_users')->where('id',$uid)->setInc('balance',$shouyi);  //余額 +

                $res = Db::name('xy_lixibao')->insert([
                    'uid'         => $uid,
                    'num'         => $shouyi,
                    'addtime'     => time(),
                    'type'        => 3,
                    'status'      => 1,
                ]);
            }
        }
    }
    
    
    //   http://gly666.15pub.net/tiyanjin_hs  每天12點執行一次
    //體驗金回收計劃
    public function tiyanjin_hs()
    {
        $tyj = Db::name('xy_users')->where('balance','>',0)->select();//獲取用戶本金大於0的
        if($tyj){
            foreach ($tyj as $v){
                $xy_recharge = Db::name('xy_recharge')->where('uid',$v['id'])->where('status',2)->count();//獲取用戶充值成功次數
                $nowtime = time() - 172800;
                $regtime = Db::name('xy_users')->where('id',$v['id'])->value('addtime');
                if($xy_recharge === 0 && $nowtime > $regtime){
                    //var_dump($v['id']);
                    Db::name('xy_users')->where('id',$v['id'])->update(['balance'=>0]);   //余額回收
                }
            }
        }
    }
}