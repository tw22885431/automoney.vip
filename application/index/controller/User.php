<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版權所有 2014~2019 
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 

// +----------------------------------------------------------------------

namespace app\index\controller;

use library\Controller;
use think\Db;

/**
 * 登錄控制器
 */
class User extends Controller
{

    protected $table = 'xy_users';

    /**
     * 空操作 用於顯示錯誤頁面
     */
    public function _empty($name){

        return $this->fetch($name);
    }

    //用戶登錄頁面
    public function login()
    {
        if(session('user_id')) $this->redirect('index/index');
        return $this->fetch();
    }

    //用戶登錄接口
    public function do_login()
    {
        // $this->applyCsrfToken();//驗證令牌
        $tel = input('post.tel/s','');
        // if(!is_mobile($tel)){
        //     return json(['code'=>1,'info'=>'手機號碼格式不正確']);
        // }
        $num = Db::table($this->table)->where(['tel'=>$tel])->count();
        if(!$num){
            return json(['code'=>1,'info'=>'賬號不存在']);
        }

        $pwd         = input('post.pwd/s', ''); 
        $keep        = input('post.keep/b', false);    
        $jizhu        = input('post.jizhu/s', 0);
		$ip = $_SERVER["REMOTE_ADDR"];

        $userinfo = Db::table($this->table)->field('id,pwd,salt,pwd_error_num,allow_login_time,status,login_status,headpic')->where('tel',$tel)->find();
        if(!$userinfo)return json(['code'=>1,'info'=>'用戶不存在']);
        if($userinfo['status'] != 1)return json(['code'=>1,'info'=>'用戶已被禁用']);
        if($userinfo['allow_login_time'] && ($userinfo['allow_login_time'] > time()) && ($userinfo['pwd_error_num'] > config('pwd_error_num')))return ['code'=>1,'info'=>'密碼連續錯誤次數太多，請'.config('allow_login_min').'分鐘後再試'];  
        if($userinfo['pwd'] != sha1($pwd.$userinfo['salt'].config('pwd_str'))){
            Db::table($this->table)->where('id',$userinfo['id'])->update(['pwd_error_num'=>Db::raw('pwd_error_num+1'),'allow_login_time'=>(time()+(config('allow_login_min') * 60))]);
            return json(['code'=>1,'info'=>'密碼錯誤']);  
        }
        
        Db::table($this->table)->where('id',$userinfo['id'])->update(['pwd_error_num'=>0,'allow_login_time'=>0,'login_status'=>1,'ip'=>$ip]);
        session('user_id',$userinfo['id']);
        session('avatar',$userinfo['headpic']);

        if ($jizhu) {
            cookie('tel',$tel);
            cookie('pwd',$pwd);
        }

        if($keep){
            Cookie::forever('user_id',$userinfo['id']);
            Cookie::forever('tel',$tel);
            Cookie::forever('pwd',$pwd);
        }
        return json(['code'=>0,'info'=>'登錄成功!']);  
    }

    /**
     * 用戶注冊接口
     */
    public function do_register()
    {
        $tel = input('post.tel/s','');
        $user_name   = input('post.user_name/s', '');
        //$user_name = '';    //交給模型隨機生成用戶名
        $verify      = input('post.verify/d', '');       //短信驗證碼
        $pwd         = input('post.pwd/s', '');
        $pwd2        = input('post.deposit_pwd/s', '');
        $invite_code = input('post.invite_code/s', '');     //邀請碼
        if(!$invite_code) return json(['code'=>1,'info'=>'邀請碼不能為空']);

        // if(config('app.verify')){
        //     $verify_msg = Db::table('xy_verify_msg')->field('msg,addtime')->where(['tel'=>$tel,'type'=>1])->find();
        //     if(!$verify_msg)return json(['code'=>1,'info'=>'驗證碼不存在']);
        //     if($verify != $verify_msg['msg'])return json(['code'=>1,'info'=>'驗證碼錯誤']);
        //     if(($verify_msg['addtime'] + (config('app.zhangjun_sms.min')*60)) < time())return json(['code'=>1,'info'=>'驗證碼已失效']);
        // }

        $pid = 0;
        if($invite_code) {
            $parentinfo = Db::table($this->table)->field('id,status')->where('invite_code',$invite_code)->find();
            if(!$parentinfo) return json(['code'=>1,'info'=>'邀請碼不存在']);
            if($parentinfo['status'] != 1) return json(['code'=>1,'info'=>'該推薦用戶已被禁用']);

            $pid = $parentinfo['id'];
        }
		$ip = $_SERVER["REMOTE_ADDR"];
        $res = model('admin/Users')->add_users($tel,$user_name,$pwd,$pid,'',$pwd2,$ip);
        return json($res);
    }


    public function logout(){
        \Session::delete('user_id');
        $this->redirect('login');
    }

    /**
     * 重置密碼
     */
    public function do_forget()
    {
        if(!request()->isPost()) return json(['code'=>1,'info'=>'錯誤請求']);
        $tel = input('post.tel/s','');
        $pwd = input('post.pwd/s','');
        $verify = input('post.verify/d',0);
        if(config('app.verify')){
            $verify_msg = Db::table('xy_verify_msg')->field('msg,addtime')->where(['tel'=>$tel,'type'=>2])->find();
            if(!$verify_msg)return json(['code'=>1,'info'=>'驗證碼不存在']);
            if($verify != $verify_msg['msg'])return json(['code'=>1,'info'=>'驗證碼錯誤']);
            if(($verify_msg['addtime'] + (config('app.zhangjun_sms.min')*60)) < time())return json(['code'=>1,'info'=>'驗證碼已失效']);
        }
        $res = model('admin/Users')->reset_pwd($tel,$pwd);
        return json($res);
    }

    public function register()
    {
        $param = \Request::param(true);
        $this->invite_code = isset($param[1]) ? trim($param[1]) : '';  
        return $this->fetch();
    }

    /*  public function reset_qrcode()
    {
        $uinfo = Db::name('xy_users')->field('id,invite_code')->select();
        foreach ($uinfo as $v) {
            $model = model('admin/Users');
            $model->create_qrcode($v['invite_code'],$v['id']);
        }
        return '重新生成用戶二維碼圖片成功';
    } */
}