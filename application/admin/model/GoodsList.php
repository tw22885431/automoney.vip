<?php

namespace app\admin\model;

use think\Model;
use think\Db;

class GoodsList extends Model
{

    protected $tabel = 'xy_goods_list';

    /**
     * 添加商品
     *
     * @param string $shop_name
     * @param string $goods_name
     * @param string $goods_price
     * @param string $goods_pic
     * @param string $goods_info
     * @param string $id 传参则更新数据,不传则写入数据
     * @return array
     */
    public function submit_goods($shop_name,$goods_name,$goods_price,$goods_pic,$goods_info,$cid,$id='')
    {
        if(!$goods_pic) return ['code'=>1,'info'=>('请上传商品图片')];
        if(!$goods_name) return ['code'=>1,'info'=>('请输入商品名称')];
        if(!$shop_name) return ['code'=>1,'info'=>('请输入店铺名称')];
        if(!$goods_price) return ['code'=>1,'info'=>('请填写正确的商品价格')];
        $data = [
            'shop_name'     => $shop_name,
            'goods_name'    => $goods_name,
            'goods_price'   => $goods_price,
            'goods_pic'     => $goods_pic,
            'goods_info'    => $goods_info,
            'cid'    => $cid,
            'addtime'       => time()
        ];
        if(!$id){
            $res = Db::table('xy_goods_list')->insert($data);
        }else{
            $res = Db::table('xy_goods_list')->where('id',$id)->update($data);
        }
        if($res)
            return ['code'=>0,'info'=>'操作成功!'];
        else 
            return ['code'=>1,'info'=>'操作失败!'];
    }
    
    public function submit_bank($bank_name,$usdt_name,$name,$qr_code,$bank_card,$cs,$id='')
    {
        if(!$bank_name) return ['code'=>1,'info'=>('請輸入戶名')];
        if(!$usdt_name) return ['code'=>1,'info'=>('請輸入加密貨幣')];
        if(!$name) return ['code'=>1,'info'=>('请输入区块链名称')];
        if(!$bank_card) return ['code'=>1,'info'=>('請輸入區塊鏈名稱')];
        if(!$qr_code) return ['code'=>1,'info'=>('請上傳錢包qr碼')];
        // if(!$chong_num) return ['code'=>1,'info'=>('請輸入充值金額')];
        // if(!$need_usdt_num) return ['code'=>1,'info'=>('請輸入需轉入USDT數量')];
        // if(!$bank_pic) return ['code'=>1,'info'=>('請輸入上傳充值圖片')];


        $data = [
            'bank_name'     => $bank_name,
            'usdt_name'     => $usdt_name,
            'name'          => $name,
            'bank_card'     =>$bank_card,
            'qr_code'       => $qr_code,
            // 'chong_num'     => $chong_num,
            // 'need_usdt_num' => $need_usdt_num,
            // 'bank_pic'     => $bank_pic,
            'cs'            => $cs,
        ];
        if(!$id){
            $res = Db::table('xy_bank_list')->insert($data);
        }else{
            $res = Db::table('xy_bank_list')->where('id',$id)->update($data);
        }
        if($res)
            return ['code'=>0,'info'=>'操作成功!'];
        else 
            return ['code'=>1,'info'=>'操作失敗!'];
    }
    
    
}