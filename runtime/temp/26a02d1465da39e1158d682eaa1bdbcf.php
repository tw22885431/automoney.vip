<?php /*a:2:{s:70:"/www/wwwroot/www.automoney.vip/application/admin/view/users/level.html";i:1598064725;s:63:"/www/wwwroot/www.automoney.vip/application/admin/view/main.html";i:1589765500;}*/ ?>
<div class="layui-card layui-bg-gray"><style>        .layui-tab-card>.layui-tab-title .layui-this {
            background-color: #fff;
        }
    </style><?php if(!(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty()))): ?><div class="layui-card-header layui-anim layui-anim-fadein notselect"><span class="layui-icon layui-icon-next font-s10 color-desc margin-right-5"></span><?php echo htmlentities((isset($title) && ($title !== '')?$title:'')); ?><div class="pull-right"><?php if(auth("add_users")): ?><!--<button data-modal='<?php echo url("add_users"); ?>' data-title="添加等级" class='layui-btn'>添加等级</button>--><?php endif; ?></div></div><?php endif; ?><div class="layui-card-body layui-anim layui-anim-upbit"><div class="think-box-shadow"><table class="layui-table margin-top-15" lay-filter="tab" lay-skin="line"><?php if(!(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty()))): ?><thead><tr><th lay-data="{field:'id',width:80}" class='text-left nowrap'>ID</th><th lay-data="{field:'name',width:80}" class='text-left nowrap'>名稱</th><th lay-data="{field:'pic',width:80}" class='text-left nowrap'>圖標</th><th lay-data="{field:'bili',width:80}" class='text-left nowrap'>傭金比例</th><th lay-data="{field:'num_min',width:120}" class='text-left nowrap'>接單最小餘額</th><th lay-data="{field:'order_num',width:80}" class='text-left nowrap'>接單次數</th><th lay-data="{field:'tixian_ci',width:80}" class='text-left nowrap'>提現次數</th><th lay-data="{field:'tixian_min',width:110}" class='text-left nowrap'>提現最小金額</th><th lay-data="{field:'tixian_max',width:110}" class='text-left nowrap'>提現最大金額</th><th lay-data="{field:'auto_vip_xu_num',width:110}" class='text-left nowrap'>陞級推薦限制</th><th lay-data="{field:'tixian_nim_order',width:110}" class='text-left nowrap'>提現訂單限制</th><th lay-data="{field:'tixian_shouxu',width:110}" class='text-left nowrap'>提現手續費</th><th lay-data="{field:'edit',width:280,fixed: 'right'}" class='text-left nowrap'>操作</th></tr></thead><?php endif; ?><tbody><?php foreach($list as $key=>$vo): ?><tr><td class='text-left nowrap'><?php echo htmlentities($vo['id']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['name']); ?></td><td class='text-left nowrap'><img src="<?php echo htmlentities($vo['pic']); ?>" alt="" style="width: 100%"></td><td class='text-left nowrap'><?php echo htmlentities($vo['bili']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['num_min']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['order_num']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['tixian_ci']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['tixian_min']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['tixian_max']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['auto_vip_xu_num']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['tixian_nim_order']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['tixian_shouxu']); ?></td><td class='text-left nowrap'><a data-dbclick class="layui-btn layui-btn-xs" data-title="會員等級" data-modal='<?php echo url("admin/users/edit_users_level"); ?>?id=<?php echo htmlentities($vo['id']); ?>'>編輯</a><a class="layui-btn layui-btn-xs layui-btn" onClick="del_user(<?php echo htmlentities($vo['id']); ?>)" style='background:red;'>删除</a></td></tr><?php endforeach; ?></tbody></table><script>        function del_user(id){
            layer.confirm("確認要删除嗎，删除後不能恢復",{ title: "删除確認" },function(index){
                $.ajax({
                    type: 'POST',
                    url: "<?php echo url('delete_user'); ?>",
                    data: {
                        'id': id,
                        '_csrf_': "<?php echo systoken('admin/users/delete_user'); ?>"
                    },
                    success:function (res) {
                        layer.msg(res.info,{time:2500});
                        location.reload();
                    }
                });
            },function(){});
        }
    </script><script>        var table = layui.table;
        //转换静态表格
        var limit = Number('<?php echo htmlentities(app('request')->get('limit')); ?>');
        if(limit==0) limit=20;
        table.init('tab', {
            cellMinWidth:120,
            skin: 'line,row',
            size: 'lg',
            limit: limit
        });
    </script><?php if(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty())): ?><span class="notdata">沒有記錄哦</span><?php else: ?><?php echo (isset($pagehtml) && ($pagehtml !== '')?$pagehtml:''); ?><?php endif; ?></div></div></div>