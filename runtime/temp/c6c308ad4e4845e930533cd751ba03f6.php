<?php /*a:2:{s:69:"/www/wwwroot/www.automoney.vip/application/admin/view/index/main.html";i:1598087204;s:63:"/www/wwwroot/www.automoney.vip/application/admin/view/main.html";i:1589765500;}*/ ?>
<div class="layui-card layui-bg-gray"><style>        .layui-tab-card>.layui-tab-title .layui-this {
            background-color: #fff;
        }
    </style><?php if(!(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty()))): ?><div class="layui-card-header layui-anim layui-anim-fadein notselect"><span class="layui-icon layui-icon-next font-s10 color-desc margin-right-5"></span><?php echo htmlentities((isset($title) && ($title !== '')?$title:'')); ?><div class="pull-right"></div></div><?php endif; ?><div class="layui-card-body layui-anim layui-anim-upbit"><style>    .store-total-container {
        font-size: 14px;
        margin-bottom: 20px;
        letter-spacing: 1px;
    }

    .store-total-container .store-total-icon {
        top: 45%;
        right: 8%;
        font-size: 65px;
        position: absolute;
        color: rgba(255, 255, 255, 0.4);
    }

    .store-total-container .store-total-item {
        color: #fff;
        line-height: 4em;
        padding: 15px 25px;
        position: relative;
    }

    .store-total-container .store-total-item > div:nth-child(2) {
        font-size: 46px;
        line-height: 46px;
    }

    .num2{font-size: 20px;font-weight: bold;line-height: 100%}
    .store-total-container .store-total-item > div:nth-child(2) {
        font-size: 26px;
        line-height: 36px;
        font-weight: bold;
    }
    .store-total-container .store-total-item{line-height: 2em}
</style><div class="think-box-shadow store-total-container notselect"><div class="margin-bottom-15">商城統計</div><div class="layui-row layui-col-space15"><div class="layui-col-sm6 layui-col-md3"><div class="store-total-item nowrap" style="background:linear-gradient(-125deg,#57bdbf,#2f9de2)"><div>商品總量</div><div><?php echo htmlentities($goods_num); ?></div><div>今日新增商品 <span class="num2"><?php echo htmlentities($today_goods_num); ?></span></div><p>昨日新增商品 <span class="num3"><?php echo htmlentities($yes_goods_num); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-template-1"></i></div><div class="layui-col-sm6 layui-col-md3"><div class="store-total-item nowrap" style="background:linear-gradient(-125deg,#ff7d7d,#fb2c95)"><div>用戶總量</div><div><?php echo htmlentities($users_num); ?></div><div>今日新增用戶 <span class="num2"><?php echo htmlentities($today_users_num); ?></span></div><p>昨日新增用戶 <span class="num3"><?php echo htmlentities($yes_users_num); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-user"></i></div><div class="layui-col-sm6 layui-col-md3"><div class="store-total-item nowrap" style="background:linear-gradient(-113deg,#c543d8,#925cc3)"><div>訂單總量</div><div><?php echo htmlentities($order_num); ?></div><div>今日新增訂單 <span class="num2"><?php echo htmlentities($today_order_num); ?></span></div><p>昨日新增訂單 <span class="num3"><?php echo htmlentities($yes_order_num); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-read"></i></div><div class="layui-col-sm6 layui-col-md3"><div class="store-total-item nowrap" style="background: linear-gradient(-113deg,#8e8cb3,#2219d0);"><div>訂單總金額</div><div><?php echo htmlentities($order_sum); ?></div><div>今日新增訂單總金額 <span class="num2"><?php echo htmlentities($today_order_sum); ?></span></div><p>昨日新增訂單總金額 <span class="num3"><?php echo htmlentities($yes_order_sum); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-rmb"></i></div><div class="layui-col-sm6 layui-col-md2"><div class="store-total-item nowrap" style="background:linear-gradient(-141deg,#ecca1b,#f39526)"><div>用戶充值</div><div><?php echo htmlentities($user_recharge); ?></div><div>今日新增充值 <span class="num2"><?php echo htmlentities($today_user_recharge); ?></span></div><p>昨日新增充值 <span class="num3"><?php echo htmlentities($yes_user_recharge); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-survey"></i></div><div class="layui-col-sm6 layui-col-md2"><div class="store-total-item nowrap" style="background: linear-gradient(-141deg,#ec4b1b,#f39526);"><div>用戶提現</div><div><?php echo htmlentities($user_deposit); ?></div><div>今日新增提現 <span class="num2"><?php echo htmlentities($today_user_deposit); ?></span></div><p>昨日新增提現 <span class="num3"><?php echo htmlentities($yes_user_deposit); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-dollar"></i></div><div class="layui-col-sm6 layui-col-md2"><div class="store-total-item nowrap" style="background: linear-gradient(-141deg,#963064,#8a0920);"><div>搶單傭金</div><div><?php echo htmlentities($user_yongjin); ?></div><div>今日新增傭金 <span class="num2"><?php echo htmlentities($today_user_yongjin); ?></span></div><p>昨日新增傭金 <span class="num3"><?php echo htmlentities($yes_user_yongjin); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-survey"></i></div><div class="layui-col-sm6 layui-col-md2"><div class="store-total-item nowrap" style="background: linear-gradient(-141deg,#1bec78,#155623);"><div>利息寶轉入</div><div><?php echo htmlentities($user_lixibao); ?></div><div>今日新增利息寶 <span class="num2"><?php echo htmlentities($today_user_lixibao); ?></span></div><p>昨日新增利息寶 <span class="num3"><?php echo htmlentities($yes_user_lixibao); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-dollar"></i></div><div class="layui-col-sm6 layui-col-md2"><div class="store-total-item nowrap" style="background: linear-gradient(-141deg,#ea7575,#abd6c6);"><div>下級返傭</div><div><?php echo htmlentities($user_fanyong); ?></div><div>今日新增傭金 <span class="num2"><?php echo htmlentities($today_user_fanyong); ?></span></div><p>昨日新增傭金 <span class="num3"><?php echo htmlentities($yes_user_fanyong); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-survey"></i></div><div class="layui-col-sm6 layui-col-md2"><div class="store-total-item nowrap" style="background: linear-gradient(-141deg,#2f3331,#565115);"><div>用戶總餘額</div><div style="font-size: 14px"><?php echo htmlentities($user_yue); ?>(<?php echo htmlentities($user_djyue); ?>)</div><div>今日利息寶轉出 <span class="num2"><?php echo htmlentities($today_lxbzc); ?></span></div><p>今日利息寶收益 <span class="num3"><?php echo htmlentities($today_lxbsy); ?></span></p></div><i class="store-total-icon layui-icon layui-icon-dollar"></i></div></div></div><div class="think-box-shadow store-total-container"><div class="margin-bottom-15">線上客服系統</div><div class="layui-row layui-col-space15"><a target="_blank" href="/customlivechat/"> 客服系統管理後臺入口</a><br><a target="_blank" href="/customlivechat/php/app.php?widget-test"> 小部件測試</a><br><a target="_blank" href="/customlivechat/php/app.php?widget-iframe-content"> 客服系統用戶端測試</a><p>新增客服系統，帳號：admin密碼：admin01</p></div></div><div class="layui-row layui-col-space15"><div class="layui-col-md6"><div class="think-box-shadow"><table class="layui-table" lay-skin="line" lay-even><caption class="text-left margin-bottom-15 font-s14">系統資訊</caption><colgroup><col width="30%"></colgroup><tbody><tr><td>當前程式版本</td><td><?php echo sysconf('app_version'); ?></td></tr><tr><td>運行PHP版本</td><td><?php echo htmlentities(PHP_VERSION); ?></td></tr><tr><td>ThinkPHP版本</td><td><?php echo htmlentities($think_ver); ?></td></tr><tr><td>MySQL資料庫版本</td><td><?php echo htmlentities($mysql_ver); ?></td></tr><tr><td>服務器作業系統</td><td><?php echo php_uname('s'); ?></td></tr><tr><td>WEB運行環境</td><td><?php echo php_sapi_name(); ?></td></tr><tr><td>上傳大小限制</td><td><?php echo ini_get('upload_max_filesize'); ?></td></tr><tr><td>POST大小限制</td><td><?php echo ini_get('post_max_size'); ?></td></tr></tbody></table></div></div><div class="layui-col-md6"><div class="think-box-shadow" id="versionTest" data-version="<?php echo htmlentities($has_version); ?>"><table class="layui-table" lay-skin="line" lay-even><caption class="text-left margin-bottom-15 font-s14">產品團隊</caption><colgroup><col width="30%"></colgroup><tbody><tr><td>產品名稱</td><td>東森國際</td></tr><tr><td>產品說明</td><td>東森國際！</td></tr><tr><td>娛樂一下</td><td><a href="#"><img src="" style="height:18px;width:auto" target="_blank"></a></td></tr><tr><td>產品大小</td><td><a target="_blank" href="">25.5M</a></td></tr><tr><td>資料庫</td><td><a target="_blank" href="">mysql</a></td></tr><tr><td>版本</td><td>V12</td></tr></tbody></table></div></div></div><script>    $(function () {
        if ($('#versionTest').data('version')) {
            setTimeout(function () {
                updateVersion();
            }, 1000);
        }
    });
</script></div></div>