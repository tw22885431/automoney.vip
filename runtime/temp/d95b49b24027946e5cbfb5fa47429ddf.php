<?php /*a:2:{s:72:"/www/wwwroot/www.automoney.vip/application/admin/view/users/tuandui.html";i:1599324986;s:63:"/www/wwwroot/www.automoney.vip/application/admin/view/main.html";i:1589765500;}*/ ?>
<div class="layui-card layui-bg-gray"><style>        .layui-tab-card>.layui-tab-title .layui-this {
            background-color: #fff;
        }
    </style><?php if(!(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty()))): ?><div class="layui-card-header layui-anim layui-anim-fadein notselect"><span class="layui-icon layui-icon-next font-s10 color-desc margin-right-5"></span><?php echo htmlentities((isset($title) && ($title !== '')?$title:'')); ?><div class="pull-right"></div></div><?php endif; ?><div class="layui-card-body layui-anim layui-anim-upbit"><div class="think-box-shadow"><div class="layui-card-header layui-anim layui-anim-fadein notselect"><span class="layui-icon layui-icon-next font-s10 color-desc margin-right-5"></span>
        當前用戶: <?php echo htmlentities($uinfo['username']); ?>(<?php echo htmlentities($uinfo['tel']); ?>)  狀態: <?php echo $uinfo['status']>0 ? '<font color="green">正常</font>' : '<font color="red">封禁</font>'; ?><div class="pull-right"></div></div><?php if(auth("open")): ?><div class="layui-form-item layui-inline" style="margin-right: 10px"><a class="layui-btn layui-btn-sm layui-btn-normal" data-open="<?php echo url('index'); ?>" data-reload="true"  data-csrf="<?php echo systoken('index'); ?>">返回會員列表</a><a class="layui-btn layui-btn-sm layui-btn-danger" data-confirm="確定要<?php echo $uinfo['status']>0 ? '封禁' : '解封'; ?>該用戶【<?php echo htmlentities($uinfo['username']); ?>】嗎？" data-action="<?php echo url('open'); ?>" data-value="id#<?php echo htmlentities($uid); ?>;status#<?php echo htmlentities($uinfo['status']); ?>;type#0" data-csrf="<?php echo systoken('open'); ?>"><?php echo $uinfo['status']>0 ? '封禁' : '解封'; ?></a><a class="layui-btn layui-btn-sm layui-btn-warning" data-confirm="确定要<?php echo $uinfo['status']>0 ? '封禁' : '解封'; ?>該用戶【<?php echo htmlentities($uinfo['username']); ?>】所有下級嗎？" data-action="<?php echo url('open'); ?>" data-value="id#<?php echo htmlentities($uid); ?>;status#<?php echo htmlentities($uinfo['status']); ?>;type#1" data-csrf="<?php echo systoken('open'); ?>"><?php echo $uinfo['status']>0 ? '封禁' : '解封'; ?>團隊</a></div><?php endif; ?><fieldset><legend>條件蒐索</legend><form class="layui-form layui-form-pane form-search" action="<?php echo request()->url(); ?>" onsubmit="return false" method="get" autocomplete="off"><div class="layui-form-item layui-inline"><label class="layui-form-label">用戶名稱</label><div class="layui-input-inline"><input name="username" value="<?php echo htmlentities((app('request')->get('username') ?: '')); ?>" placeholder="請輸入用戶名稱" class="layui-input"></div></div><div class="layui-form-item layui-inline"><label class="layui-form-label">手機號碼</label><div class="layui-input-inline"><input name="tel" value="<?php echo htmlentities((app('request')->get('tel') ?: '')); ?>" placeholder="請輸入手機號碼" class="layui-input"></div></div><div class="layui-form-item layui-inline"><label class="layui-form-label">註冊時間</label><div class="layui-input-inline"><input data-date-range name="addtime" value="<?php echo htmlentities((app('request')->get('addtime') ?: '')); ?>" placeholder="請選擇註冊時間" class="layui-input"></div></div><div class="layui-form-item layui-inline"><button class="layui-btn layui-btn-primary"><i class="layui-icon">&#xe615;</i> 蒐 索</button></div></form></fieldset><div class="layui-tab"><ul class="layui-tab-title"><li class="layui-this">Ta的下線</li><li class="">一級會員</li><li>二級會員</li><li>三級會員</li><li>四級會員</li><li>五級會員</li></ul><div class="layui-tab-content"><div class="layui-tab-item layui-show"><table id="demo0" lay-filter="test1"></table></div><div class="layui-tab-item"><table id="demo1" lay-filter="test2"></table></div><div class="layui-tab-item"><table id="demo2" lay-filter="test2"></table></div><div class="layui-tab-item"><table id="demo3" lay-filter="test3"></table></div><div class="layui-tab-item"><table id="demo4" lay-filter="test2"></table></div><div class="layui-tab-item"><table id="demo5" lay-filter="test3"></table></div></div></div><script>
        $(function () {
            var table = layui.table;

            //第一个实例
            table.render({
                elem: '#demo0'
                ,where: {
                    iasjax:1,
                    level:-1,
                    addtime:"<?php echo htmlentities(app('request')->get('addtime')); ?>",
                    tel:"<?php echo htmlentities(app('request')->get('tel')); ?>",
                    username:"<?php echo htmlentities(app('request')->get('username')); ?>",
                    id:<?php echo htmlentities($uid); ?>
                }
                ,height: 512
                ,url: '/admin/users/tuandui' //数据接口
                ,totalRow: true
                ,page: true //开启分页
                ,cols: [[ //表头
                    {field: 'id', title: 'ID', width:50, sort: true, fixed: 'left',}
                    ,{field: 'tel', title: '帳號',width:120 ,totalRowText: '合計'}
                     ,{field: 'name', title: '會員名稱' }
                    ,{field: 'balance', title: '餘額',sort: true,totalRow: true}
                    ,{field: 'yj', title: '傭金',sort: true,totalRow: true}
                    ,{field: 'cz', title: '充值',totalRow: true}
                    ,{field: 'tx', title: '提現',totalRow: true}
                    ,{field: 'jb', title: '級別',sort:true }
                    ,{field: 'parent_name', title: '上級用戶' }
                   
                    ,{field: 'childs', title: '直推人數',totalRow: true }
                    ,{field: 'addtime', title: '註冊時間',width:150 }
                    ,{field: 'id', title: '操作',width:170, templet:function (d) {
                        var name = d.status ? "封禁":"解封";
                        return '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-title="查看賬變" data-reload="true" data-open="<?php echo url('admin/users/caiwu'); ?>?id='+d.id+'" style="background: #dc1ed1">賬變</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-confirm="確定要'+name+'該用戶【'+d.username+'】嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#0" data-csrf="<?php echo systoken('open'); ?>" style="background: '+color+'">'+name+'</a>' +
                            '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-warning"  data-confirm="確定要封禁該用戶【'+d.username+'】所有下級嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#1" data-csrf="<?php echo systoken('open'); ?>">'+name+'團隊</a>'
                        var color = d.status ? "red":"green";
                    }}
                ]]
            });

            //<th lay-data="{field:'id',width:50}" class='text-left nowrap'>ID</th>
            $('.layui-tab-title li').click(function () {
                if( $(this).text() == '一級會員') {

                    //第2个实例
                    table.render({
                        elem: '#demo1'
                        ,where: {
                            iasjax:1,
                            level:1,
                            addtime:"<?php echo htmlentities(app('request')->get('addtime')); ?>",
                            tel:"<?php echo htmlentities(app('request')->get('tel')); ?>",
                            username:"<?php echo htmlentities(app('request')->get('username')); ?>",
                            id:<?php echo htmlentities($uid); ?>
                        }
                        ,height: 512
                        ,url: '/admin/users/tuandui' //数据接口
                        ,totalRow: true
                        ,page: true //开启分页
                        ,cols: [[ //表头
                            {field: 'id', title: 'ID', width:50, sort: true, fixed: 'left',}
                            ,{field: 'tel', title: '帳號',width:120 ,totalRowText: '合計'}
                            ,{field: 'username', title: '會員名稱' }
                            ,{field: 'balance', title: '餘額',sort: true,totalRow: true}
                            ,{field: 'yj', title: '傭金',sort: true,totalRow: true}
                            ,{field: 'cz', title: '充值',totalRow: true}
                            ,{field: 'tx', title: '提現',totalRow: true}
                            ,{field: 'jb', title: '級別',sort:true }
                            ,{field: 'parent_name', title: '上級用戶' }
                            ,{field: 'childs', title: '直推人數',totalRow: true }
                            ,{field: 'addtime', title: '註冊時間',width:150 }
                            ,{field: 'id', title: '操作',width:170, templet:function (d) {
                                var name = d.status ? "封禁":"解封";
                                       return '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-title="查看賬變" data-reload="true" data-open="<?php echo url('admin/users/caiwu'); ?>?id='+d.id+'" style="background: #dc1ed1">賬變</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-confirm="確定要'+name+'該用戶【'+d.username+'】嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#0" data-csrf="<?php echo systoken('open'); ?>" style="background: '+color+'">'+name+'</a>' +
                            '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-warning"  data-confirm="確定要封禁該用戶【'+d.username+'】所有下級嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#1" data-csrf="<?php echo systoken('open'); ?>">'+name+'團隊</a>'
                        var color = d.status ? "red":"green";
                            }}
                        ]]
                    });

                }else if( $(this).text() == '二級會員') {

                    //第3个实例
                    table.render({
                        elem: '#demo2'
                        ,where: {
                            iasjax:1,
                            level:2,
                            addtime:"<?php echo htmlentities(app('request')->get('addtime')); ?>",
                            tel:"<?php echo htmlentities(app('request')->get('tel')); ?>",
                            username:"<?php echo htmlentities(app('request')->get('username')); ?>",
                            id:<?php echo htmlentities($uid); ?>
                        }
                        ,height: 512
                        ,url: '/admin/users/tuandui' //数据接口
                        ,totalRow: true
                        ,page: true //开启分页
                        ,cols: [[ //表头
                            {field: 'id', title: 'ID', width:50, sort: true, fixed: 'left',}
                            ,{field: 'tel', title: '帳號',width:120 ,totalRowText: '合計'}
                            ,{field: 'username', title: '會員名稱' }
                                           ,{field: 'balance', title: '餘額',sort: true,totalRow: true}
                    ,{field: 'yj', title: '傭金',sort: true,totalRow: true}
                    ,{field: 'cz', title: '充值',totalRow: true}
                    ,{field: 'tx', title: '提現',totalRow: true}
                    ,{field: 'jb', title: '級別',sort:true }
                    ,{field: 'parent_name', title: '上級用戶' }
                    ,{field: 'childs', title: '直推人數',totalRow: true }
                    ,{field: 'addtime', title: '註冊時間',width:150 }
                            ,{field: 'id', title: '操作',width:170, templet:function (d) {
                                var name = d.status ? "封禁":"解封";
                                  return '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-title="查看賬變" data-reload="true" data-open="<?php echo url('admin/users/caiwu'); ?>?id='+d.id+'" style="background: #dc1ed1">賬變</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-confirm="確定要'+name+'該用戶【'+d.username+'】嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#0" data-csrf="<?php echo systoken('open'); ?>" style="background: '+color+'">'+name+'</a>' +
                            '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-warning"  data-confirm="確定要封禁該用戶【'+d.username+'】所有下級嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#1" data-csrf="<?php echo systoken('open'); ?>">'+name+'團隊</a>'
                        var color = d.status ? "red":"green";
                            }}
                        ]]
                    });

                }else if( $(this).text() == '三級會員') {

                    //第4个实例
                    table.render({
                        elem: '#demo3'
                        ,where: {
                            iasjax:1,
                            level:3,
                            addtime:"<?php echo htmlentities(app('request')->get('addtime')); ?>",
                            tel:"<?php echo htmlentities(app('request')->get('tel')); ?>",
                            username:"<?php echo htmlentities(app('request')->get('username')); ?>",
                            id:<?php echo htmlentities($uid); ?>
                        }
                        ,height: 512
                        ,url: '/admin/users/tuandui' //数据接口
                        ,totalRow: true
                        ,page: true //开启分页
                        ,cols: [[ //表头
                            {field: 'id', title: 'ID', width:50, sort: true, fixed: 'left',}
                            ,{field: 'tel', title: '帳號',width:120 ,totalRowText: '合計'}
                            ,{field: 'username', title: '會員名稱' }
                    ,{field: 'balance', title: '餘額',sort: true,totalRow: true}
                    ,{field: 'yj', title: '傭金',sort: true,totalRow: true}
                    ,{field: 'cz', title: '充值',totalRow: true}
                    ,{field: 'tx', title: '提現',totalRow: true}
                    ,{field: 'jb', title: '級別',sort:true }
                    ,{field: 'parent_name', title: '上級用戶' }
                    ,{field: 'childs', title: '直推人數',totalRow: true }
                    ,{field: 'addtime', title: '註冊時間',width:150 }
                            ,{field: 'id', title: '操作',width:170, templet:function (d) {
                                var name = d.status ? "封禁":"解封";
                              return '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-title="查看賬變" data-reload="true" data-open="<?php echo url('admin/users/caiwu'); ?>?id='+d.id+'" style="background: #dc1ed1">賬變</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-confirm="確定要'+name+'該用戶【'+d.username+'】嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#0" data-csrf="<?php echo systoken('open'); ?>" style="background: '+color+'">'+name+'</a>' +
                            '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-warning"  data-confirm="確定要封禁該用戶【'+d.username+'】所有下級嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#1" data-csrf="<?php echo systoken('open'); ?>">'+name+'團隊</a>'
                        var color = d.status ? "red":"green";
                            }}
                        ]]
                    });
                }else if( $(this).text() == '四級會員') {

                    //第4个实例
                    table.render({
                        elem: '#demo4'
                        ,where: {
                            iasjax:1,
                            level:4,
                            addtime:"<?php echo htmlentities(app('request')->get('addtime')); ?>",
                            tel:"<?php echo htmlentities(app('request')->get('tel')); ?>",
                            username:"<?php echo htmlentities(app('request')->get('username')); ?>",
                            id:<?php echo htmlentities($uid); ?>
                        }
                        ,height: 512
                        ,url: '/admin/users/tuandui' //数据接口
                        ,totalRow: true
                        ,page: true //开启分页
                        ,cols: [[ //表头
                            {field: 'id', title: 'ID', width:50, sort: true, fixed: 'left',}
                            ,{field: 'tel', title: '帳號',width:120 ,totalRowText: '合計'}
                            ,{field: 'username', title: '會員名稱' }
                    ,{field: 'balance', title: '餘額',sort: true,totalRow: true}
                    ,{field: 'yj', title: '傭金',sort: true,totalRow: true}
                    ,{field: 'cz', title: '充值',totalRow: true}
                    ,{field: 'tx', title: '提現',totalRow: true}
                    ,{field: 'jb', title: '級別',sort:true }
                    ,{field: 'parent_name', title: '上級用戶' }
                    ,{field: 'childs', title: '直推人數',totalRow: true }
                    ,{field: 'addtime', title: '註冊時間',width:150 }
                            ,{field: 'id', title: '操作',width:170, templet:function (d) {
                                var name = d.status ? "封禁":"解封";
                                        return '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-title="查看賬變" data-reload="true" data-open="<?php echo url('admin/users/caiwu'); ?>?id='+d.id+'" style="background: #dc1ed1">賬變</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-confirm="確定要'+name+'該用戶【'+d.username+'】嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#0" data-csrf="<?php echo systoken('open'); ?>" style="background: '+color+'">'+name+'</a>' +
                            '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-warning"  data-confirm="確定要封禁該用戶【'+d.username+'】所有下級嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#1" data-csrf="<?php echo systoken('open'); ?>">'+name+'團隊</a>'
                        var color = d.status ? "red":"green";
                            }}
                        ]]
                    });
                }else if( $(this).text() == '五級會員') {

                    //第4个实例
                    table.render({
                        elem: '#demo5'
                        ,where: {
                            iasjax:1,
                            level:5,
                            addtime:"<?php echo htmlentities(app('request')->get('addtime')); ?>",
                            tel:"<?php echo htmlentities(app('request')->get('tel')); ?>",
                            username:"<?php echo htmlentities(app('request')->get('username')); ?>",
                            id:<?php echo htmlentities($uid); ?>
                        }
                        ,height: 512
                        ,url: '/admin/users/tuandui' //数据接口
                        ,totalRow: true
                        ,page: true //开启分页
                        ,cols: [[ //表头
                            {field: 'id', title: 'ID', width:50, sort: true, fixed: 'left',}
                            ,{field: 'tel', title: '帳號',width:120 ,totalRowText: '合計'}
                            ,{field: 'username', title: '會員名稱' }
                        ,{field: 'balance', title: '餘額',sort: true,totalRow: true}
                    ,{field: 'yj', title: '傭金',sort: true,totalRow: true}
                    ,{field: 'cz', title: '充值',totalRow: true}
                    ,{field: 'tx', title: '提現',totalRow: true}
                    ,{field: 'jb', title: '級別',sort:true }
                    ,{field: 'parent_name', title: '上級用戶' }
                    ,{field: 'childs', title: '直推人數',totalRow: true }
                    ,{field: 'addtime', title: '註冊時間',width:150 }
                            ,{field: 'id', title: '操作',width:170, templet:function (d) {
                                var name = d.status ? "封禁":"解封";
                                        return '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-title="查看賬變" data-reload="true" data-open="<?php echo url('admin/users/caiwu'); ?>?id='+d.id+'" style="background: #dc1ed1">賬變</a>' +
                            '<a class="layui-btn layui-btn-xs layui-btn-danger"  data-confirm="確定要'+name+'該用戶【'+d.username+'】嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#0" data-csrf="<?php echo systoken('open'); ?>" style="background: '+color+'">'+name+'</a>' +
                            '' +
                            '<a class="layui-btn layui-btn-xs layui-btn-warning"  data-confirm="確定要封禁該用戶【'+d.username+'】所有下級嗎？" data-action="<?php echo url('open'); ?>" data-value="id#'+d.id+';status#'+d.status+';type#1" data-csrf="<?php echo systoken('open'); ?>">'+name+'團隊</a>'
                        var color = d.status ? "red":"green";
                            }}
                        ]]
                    });
                }
            })
        })
    </script></div></div></div>