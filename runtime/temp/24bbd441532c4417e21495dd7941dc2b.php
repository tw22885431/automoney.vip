<?php /*a:2:{s:70:"/www/wwwroot/www.automoney.vip/application/admin/view/config/info.html";i:1598022955;s:63:"/www/wwwroot/www.automoney.vip/application/admin/view/main.html";i:1589765500;}*/ ?>
<div class="layui-card layui-bg-gray"><style>    .right-btn {
        top: 0;
        right: 0;
        width: 38px;
        height: 38px;
        display: inline-block;
        position: absolute;
        text-align: center;
        line-height: 38px;
    }
</style><script>    window.form.render();
</script><script>    function clearData(){
        $.msg.confirm('確定要清理嗎？', function (index) {
            //history.back();
            $.msg.close(index);
            var chk_value =[];
            $('input[name="clear"]:checked').each(function(){
                chk_value.push($(this).val());
            });

            console.log(chk_value)
            console.log(JSON.stringify( chk_value ))
            $.ajax({
                type: 'POST',
                url: "<?php echo url('clear'); ?>",
                data: {
                    'id': 2,
                    'data':JSON.stringify( chk_value ),
                    '_csrf_': "<?php echo systoken('admin/config/clear'); ?>"
                },
                success:function (res) {
                    layer.msg(res.info,{time:2500});
                    //location.reload();
                }
            });



        },function () {
            return false;
        });


    }
</script><style>        .layui-tab-card>.layui-tab-title .layui-this {
            background-color: #fff;
        }
    </style><?php if(!(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty()))): ?><div class="layui-card-header layui-anim layui-anim-fadein notselect"><span class="layui-icon layui-icon-next font-s10 color-desc margin-right-5"></span><?php echo htmlentities((isset($title) && ($title !== '')?$title:'')); ?><div class="pull-right"></div></div><?php endif; ?><div class="layui-card-body layui-anim layui-anim-upbit"><?php if(auth('config')): ?><div class="think-box-shadow margin-bottom-15"><span class="color-green font-w7 text-middle">數據清理：</span><div class="layui-form layui-card" ><div class="layui-card-body"><div class="layui-form-item" pane=""><label class="layui-form-label">顯示選項</label><div class="layui-input-block" id="clear"><input type="checkbox" name="clear" value="1" lay-skin="primary" title="用户" ><div class="layui-unselect layui-form-checkbox layui-form-checked" lay-skin="primary"><span>用户</span><i class="layui-icon layui-icon-ok"></i></div><input type="checkbox" name="clear" value="2" lay-skin="primary" title="交易" ><div class="layui-unselect layui-form-checkbox layui-form-checked" lay-skin="primary"><span>交易</span><i class="layui-icon layui-icon-ok"></i></div><input type="checkbox" name="clear" value="3" lay-skin="primary" title="財務記錄" ><div class="layui-unselect layui-form-checkbox layui-form-checked" lay-skin="primary"><span>財務記錄</span><i class="layui-icon layui-icon-ok"></i></div><input type="checkbox" name="clear" value="4" lay-skin="primary" title="充值" ><div class="layui-unselect layui-form-checkbox layui-form-checked" lay-skin="primary"><span>充值</span><i class="layui-icon layui-icon-ok"></i></div><input type="checkbox" name="clear" value="5" lay-skin="primary" title="提現" ><div class="layui-unselect layui-form-checkbox layui-form-checked" lay-skin="primary"><span>提現</span><i class="layui-icon layui-icon-ok"></i></div><input type="checkbox" name="clear" value="6" lay-skin="primary" title="銀行卡" ><div class="layui-unselect layui-form-checkbox layui-form-checked" lay-skin="primary"><span>銀行卡</span><i class="layui-icon layui-icon-ok"></i></div><input type="checkbox" name="clear" value="7" lay-skin="primary" title="地址" ><div class="layui-unselect layui-form-checkbox layui-form-checked" lay-skin="primary"><span>地址</span><i class="layui-icon layui-icon-ok"></i></div><input type="checkbox" name="clear" value="8" lay-skin="primary" title="利息寶" ><div class="layui-unselect layui-form-checkbox layui-form-checked" lay-skin="primary"><span>利息寶</span><i class="layui-icon layui-icon-ok"></i></div></div></div></div></div><a class="layui-btn layui-btn-sm" onclick="clearData()" >確定清理</a></div><?php endif; if(auth('config')): ?><div class="think-box-shadow margin-bottom-15"><span class="color-green font-w7 text-middle">系統參數配寘：</span><a class="layui-btn layui-btn-sm" data-modal="<?php echo url('config'); ?>">修改系統配寘</a></div><?php endif; ?><div class="think-box-shadow margin-bottom-15"><span class="color-green font-w7 text-middle">檔存儲引擎：</span><?php foreach(['local'=>'本地服務器存儲','qiniu'=>'七牛雲對象存儲','oss'=>'阿裡雲OSS存儲'] as $k=>$v): if(sysconf('storage_type') == $k): if(auth('file')): ?><a data-modal="<?php echo url('file'); ?>?type=<?php echo htmlentities($k); ?>" class="layui-btn layui-btn-sm"><?php echo htmlentities($v); ?></a><?php else: ?><a class="layui-btn layui-btn-sm"><?php echo htmlentities($v); ?></a><?php endif; elseif(auth('file')): ?><a data-modal="<?php echo url('file'); ?>?type=<?php echo htmlentities($k); ?>" class="layui-btn layui-btn-sm layui-btn-primary"><?php echo htmlentities($v); ?></a><?php endif; ?><?php endforeach; ?></div><div class="think-box-shadow padding-40"><div class="layui-form-item"><span class="color-green font-w7">網站名稱 Website</span><label class="relative block"><input readonly value="<?php echo sysconf('site_name'); ?>" class="layui-input layui-bg-gray"><a data-copy="<?php echo sysconf('site_name'); ?>" class="fa fa-copy right-btn"></a></label><p class="help-block">網站名稱及網站圖標，將顯示在瀏覽器的標籤上</p></div><div class="layui-form-item"><span class="color-green font-w7">網站名稱 Website</span><label class="relative block"><input readonly value="<?php echo sysconf('site_name'); ?>" class="layui-input layui-bg-gray"><a data-copy="<?php echo sysconf('site_name'); ?>" class="fa fa-copy right-btn"></a></label><p class="help-block">網站名稱及網站圖標，將顯示在瀏覽器的標籤上</p></div><div class="layui-form-item"><span class="color-green font-w7">管理程式名稱 Name</span><label class="relative block"><input readonly placeholder="請輸入程式名稱" value="<?php echo sysconf('app_name'); ?>" class="layui-input layui-bg-gray"><a data-copy="<?php echo sysconf('app_name'); ?>" class="fa fa-copy right-btn"></a></label><p class="help-block">管理程式名稱，將顯示在後臺左上角標題</p></div><div class="layui-form-item"><span class="color-green font-w7">管理程式版本 Version</span><label class="relative block"><input readonly value="<?php echo sysconf('app_version'); ?>" class="layui-input layui-bg-gray"><a data-copy="<?php echo sysconf('app_version'); ?>" class="fa fa-copy right-btn"></a></label><p class="help-block">管理程式版本，將顯示在後臺左上角標題</p></div><div class="layui-form-item"><span class="color-green font-w7">網站備案號 Miitbeian</span><label class="relative block"><input readonly value="<?php echo sysconf('miitbeian'); ?>" class="layui-input layui-bg-gray"><a data-copy="<?php echo sysconf('miitbeian'); ?>" class="fa fa-copy right-btn"></a></label><p class="help-block">網站備案號，可以在<a target="_blank" href="http://beian.miit.gov.cn">備案管理中心</a>査詢獲取，將顯示在登入頁面下麵</p></div><div class="layui-form-item"><span class="color-green font-w7">網站版權資訊 Copyright</span><label class="relative block"><input readonly value="<?php echo sysconf('site_copy'); ?>" class="layui-input layui-bg-gray"><a data-copy="<?php echo sysconf('site_copy'); ?>" class="fa fa-copy right-btn"></a></label><p class="help-block">網站版權資訊，在後臺登入頁面顯示版本資訊並連結到備案到資訊備案管理系統</p></div></div></div></div>