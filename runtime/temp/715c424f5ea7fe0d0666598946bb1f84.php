<?php /*a:2:{s:76:"/www/wwwroot/www.automoney.vip/application/admin/view/deal/lixibao_list.html";i:1598029391;s:63:"/www/wwwroot/www.automoney.vip/application/admin/view/main.html";i:1589765500;}*/ ?>
<div class="layui-card layui-bg-gray"><style>        .layui-tab-card>.layui-tab-title .layui-this {
            background-color: #fff;
        }
    </style><?php if(!(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty()))): ?><div class="layui-card-header layui-anim layui-anim-fadein notselect"><span class="layui-icon layui-icon-next font-s10 color-desc margin-right-5"></span><?php echo htmlentities((isset($title) && ($title !== '')?$title:'')); ?><div class="pull-right"><?php if(auth("add_lixibao")): ?><button data-open='<?php echo url("add_lixibao"); ?>' data-title="添加选项" class='layui-btn'>添加选项</button><?php endif; ?></div></div><?php endif; ?><div class="layui-card-body layui-anim layui-anim-upbit"><div class="think-box-shadow"><fieldset><legend>條件蒐索</legend><form class="layui-form layui-form-pane form-search" action="<?php echo request()->url(); ?>" onsubmit="return false" method="get" autocomplete="off"><div class="layui-form-item layui-inline"><label class="layui-form-label">發起時間</label><div class="layui-input-inline"><input data-date-range name="addtime" value="<?php echo htmlentities((app('request')->get('addtime') ?: '')); ?>" placeholder="請選擇發起時間" class="layui-input"></div></div><div class="layui-form-item layui-inline"><button class="layui-btn layui-btn-primary"><i class="layui-icon">&#xe615;</i> 蒐 索</button></div></form></fieldset><script>form.render()</script><table class="layui-table margin-top-15" lay-skin="line"><?php if(!(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty()))): ?><thead><tr><th class='list-table-check-td think-checkbox'><input data-auto-none data-check-target='.list-check-box' type='checkbox'></th><th class='text-left nowrap'>編號</th><th class='text-left nowrap'>產品名稱</th><th class='text-left nowrap'>期限</th><th class='text-left nowrap'>利率</th><th class='text-left nowrap'>手續費</th><th class='text-left nowrap'>最低限制金額</th><th class='text-left nowrap'>最高限制金額</th><th class='text-left nowrap'>狀態</th><th class='text-left nowrap'>提交時間</th><th class='text-left nowrap'>操作</th></tr></thead><?php endif; ?><tbody><?php foreach($list as $key=>$vo): ?><tr><td class='list-table-check-td think-checkbox'><input class="list-check-box" value='<?php echo htmlentities($vo['id']); ?>' type='checkbox'></td><td class='text-left nowrap'><?php echo htmlentities($vo['id']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['name']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['day']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['bili']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['shouxu']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['min_num']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['max_num']); ?></td><td class='text-left nowrap'><?php switch($vo['status']): case "1": ?>啟用<?php break; case "0": ?>未啟用<?php break; ?><?php endswitch; ?></td><td class='text-left nowrap'><?php echo htmlentities(format_datetime($vo['addtime'])); ?></td><td class='text-left nowrap'><?php if($vo['status'] == 1): ?><a class="layui-btn layui-btn-warm layui-btn-xs" data-action="<?php echo url('lxb_forbid'); ?>" data-value="id#<?php echo htmlentities($vo['id']); ?>;status#0" data-csrf="<?php echo systoken('admin/deal/lxb_forbid'); ?>">禁 用</a><?php elseif($vo['status'] == 0): ?><a class="layui-btn layui-btn-warm layui-btn-xs" data-action="<?php echo url('lxb_resume'); ?>" data-value="id#<?php echo htmlentities($vo['id']); ?>;status#1" data-csrf="<?php echo systoken('admin/deal/lxb_resume'); ?>">啟 用</a><?php endif; if(auth("edit_lixibao")): ?><a class="layui-btn layui-btn-xs layui-btn" data-open="<?php echo url('edit_lixibao',['id'=>$vo['id']]); ?>" data-value="id#<?php echo htmlentities($vo['id']); ?>" style='background:green;'>編輯</a><?php endif; if(auth("del_lixibao")): ?><a class="layui-btn layui-btn-xs layui-btn" style='background:red;' onClick="del_goods(<?php echo htmlentities($vo['id']); ?>)">删除</a><?php endif; ?></td></tr><?php endforeach; ?></tbody></table><?php if(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty())): ?><span class="notdata">沒有記錄哦</span><?php else: ?><?php echo (isset($pagehtml) && ($pagehtml !== '')?$pagehtml:''); ?><?php endif; ?><script>
        var test = "<?php echo htmlentities((app('request')->get('type') ?: '0')); ?>";
        $("#selectList").find("option[value="+test+"]").prop("selected",true);

        form.render()



    </script><script>
        function del_goods(id){
            layer.confirm("確認要删除嗎，删除後不能恢復",{ title: "删除確認" },function(index){
                $.ajax({
                    type: 'POST',
                    url: "<?php echo url('del_lixibao'); ?>",
                    data: {
                        'id': id,
                        '_csrf_': "<?php echo systoken('admin/deal/del_lixibao'); ?>"
                    },
                    success:function (res) {
                        layer.msg(res.info,{time:2500});
                        location.reload();
                    }
                });
            },function(){});
        }
    </script></div></div></div>