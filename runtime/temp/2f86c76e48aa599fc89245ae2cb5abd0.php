<?php /*a:2:{s:73:"/www/wwwroot/www.automoney.vip/application/admin/view/deal/bank_list.html";i:1598547800;s:63:"/www/wwwroot/www.automoney.vip/application/admin/view/main.html";i:1589765500;}*/ ?>
<div class="layui-card layui-bg-gray"><style>        .layui-tab-card>.layui-tab-title .layui-this {
            background-color: #fff;
        }
    </style><?php if(!(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty()))): ?><div class="layui-card-header layui-anim layui-anim-fadein notselect"><span class="layui-icon layui-icon-next font-s10 color-desc margin-right-5"></span><?php echo htmlentities((isset($title) && ($title !== '')?$title:'')); ?><div class="pull-right"><?php if(auth("add_bank")): ?><button data-open='<?php echo url("add_bank"); ?>' data-title="添加卡号" class='layui-btn'>添加錢包</button><?php endif; ?></div></div><?php endif; ?><div class="layui-card-body layui-anim layui-anim-upbit"><div class="think-box-shadow"><table class="layui-table margin-top-15" lay-skin="line"><?php if(!(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty()))): ?><thead><tr><th class='text-left nowrap'>ID</th><th class='text-left nowrap'>戶名</th><th class='text-left nowrap'>加密貨幣</th><th class='text-left nowrap'>區塊鏈名稱</th><th class='text-left nowrap'>錢包qr碼</th><th class='text-left nowrap'>錢包地址</th><th class='text-left nowrap'>操作</th></tr></thead><?php endif; ?><tbody><?php foreach($list as $key=>$vo): ?><tr><td class='text-left nowrap'><?php echo htmlentities($vo['id']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['bank_name']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['usdt_name']); ?></td><td class='text-left nowrap'><?php echo htmlentities($vo['name']); ?></td><td class='text-left nowrap'><img src="<?php echo htmlentities($vo['qr_code']); ?>" alt="" style="width: 80px"></td><td class='text-left nowrap'><?php echo htmlentities($vo['bank_card']); ?></td><td class='text-left nowrap'><?php if(auth("edit_bank")): ?><a class="layui-btn layui-btn-xs layui-btn" data-open="<?php echo url('edit_bank',['id'=>$vo['id']]); ?>" data-value="id#<?php echo htmlentities($vo['id']); ?>" style='background:green;'>編輯</a><?php endif; if(auth("del_bank")): ?><a class="layui-btn layui-btn-xs layui-btn" style='background:red;' onClick="del_bank(<?php echo htmlentities($vo['id']); ?>)">删除</a><?php endif; ?></td></tr><?php endforeach; ?></tbody></table><?php if(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty())): ?><span class="notdata">沒有記錄哦</span><?php else: ?><?php echo (isset($pagehtml) && ($pagehtml !== '')?$pagehtml:''); ?><?php endif; ?></div><script>
    function del_bank(id){
        layer.confirm("確認要删除嗎，删除後不能恢復",{ title: "删除确认" },function(index){
            $.ajax({
                type: 'POST',
                url: "<?php echo url('del_bank'); ?>",
                data: {
                    'id': id,
                    '_csrf_': "<?php echo systoken('admin/deal/del_bank'); ?>"
                },
                success:function (res) {
                    layer.msg(res.info,{time:2500});
                    location.reload();
                }
            });
        },function(){});
    }
</script></div></div>