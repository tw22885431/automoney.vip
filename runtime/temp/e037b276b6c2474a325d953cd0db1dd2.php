<?php /*a:2:{s:73:"/www/wwwroot/www.automoney.vip/application/admin/view/deal/edit_cate.html";i:1598544966;s:63:"/www/wwwroot/www.automoney.vip/application/admin/view/main.html";i:1589765500;}*/ ?>
<div class="layui-card layui-bg-gray"><style>        .layui-tab-card>.layui-tab-title .layui-this {
            background-color: #fff;
        }
    </style><?php if(!(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty()))): ?><div class="layui-card-header layui-anim layui-anim-fadein notselect"><span class="layui-icon layui-icon-next font-s10 color-desc margin-right-5"></span><?php echo htmlentities((isset($title) && ($title !== '')?$title:'')); ?><div class="pull-right"></div></div><?php endif; ?><div class="layui-card-body layui-anim layui-anim-upbit"><form onsubmit="return false;" id="GoodsForm" data-auto="true" method="post" class='layui-form layui-card' autocomplete="off"><div class="layui-card-body think-box-shadow padding-left-40"><div class="layui-form-item layui-row layui-col-space15"><label class="layui-col-xs9 relative"><span class="color-green">分類名稱</span><input name="name" required class="layui-input" value="<?php echo htmlentities($info['name']); ?>" placeholder="請輸入分類名稱"><input type="hidden" name="id" value="<?php echo htmlentities($info['id']); ?>"><input type="hidden" name="_csrf_" value="<?php echo systoken('admin/deal/edit_cate'); ?>"></label></div><div class="layui-form-item layui-row layui-col-space15"><label class="layui-col-xs9 relative"><span class="color-green">綁定會員等級</span><select name="level" id="selectList" readonly="readonly" ><?php foreach($level as $key=>$vo): ?><option value="<?php echo htmlentities($vo['level']); ?>" <?php echo $info['level_id']==$vo['id'] ? 'selected': ''; ?>><?php echo htmlentities($vo['name']); ?></option><?php endforeach; ?></select></label></div><div class="layui-form-item layui-row layui-col-space15"><label class="layui-col-xs9 relative"><span class="color-green">比例</span><input name="bili" required class="layui-input" value="<?php echo htmlentities($info['bili']); ?>" placeholder="比例以會員等級為主" readonly></label></div><div class="layui-form-item layui-row layui-col-space15"><label class="layui-col-xs9 relative"><span class="color-green">簡介</span><input name="cate_info" required class="layui-input" value="<?php echo htmlentities($info['cate_info']); ?>" placeholder="請輸入簡介"></label></div><div class="layui-form-item layui-row layui-col-space15"><label class="layui-col-xs9 relative"><span class="color-green">最低金額限制</span><input name="min" required class="layui-input" value="<?php echo htmlentities($info['min']); ?>" placeholder="請輸入金額"></label></div><div class="layui-form-item"><span class="color-green label-required-prev">分類LOGO（不用修改）</span><table class="layui-table"><thead><tr><th class="text-center">展示圖片</th></tr><tr><td width="90px" class="text-center"><input name="cate_pic" value="<?php echo htmlentities($info['cate_pic']); ?>" type="hidden"></td></tr></thead></table><script>$('[name="cate_pic"]').uploadOneImage();</script></div><div class="layui-form-item block"></div><div class="layui-form-item text-center"><button class="layui-btn" type="submit">保存</button><button class="layui-btn layui-btn-danger" ng-click="hsitoryBack()" type="button">取消編輯</button></div></div></form></div><script>
    window.form.render();
    require(['ckeditor', 'angular'], function () {
        window.createEditor('[name="goods_info"]', {height: 500});
        var app = angular.module("GoodsForm", []).run(callback);
        angular.bootstrap(document.getElementById(app.name), [app.name]);

        function callback($rootScope) {
            $rootScope.isAddMode = parseInt('<?php echo htmlentities((isset($isAddMode) && ($isAddMode !== '')?$isAddMode:0)); ?>');
            $rootScope.maps = JSON.parse(angular.element('#goods-value').val() || '[]') || {};
            $rootScope.specs = JSON.parse(angular.element('#goods-specs').val() || '[{"name":"默認分組","list":[{"name":"默認規格","check":true}]}]');
            // 批量设置数值
            $rootScope.batchSet = function (type, fixed) {
                layer.prompt({title: '請輸入數值', formType: 0}, function (value, index) {
                    $rootScope.$apply(function () {
                        var val = (parseFloat(value) || 0).toFixed(fixed);
                        for (var i in $rootScope.specsTreeData) for (var j in $rootScope.specsTreeData[i]) {
                            $rootScope.specsTreeData[i][j][type] = val;
                        }
                    });
                    layer.close(index);
                });
            };
            // 返回商品列表
            $rootScope.hsitoryBack = function () {
                $.msg.confirm('確定要取消編輯嗎？', function (index) {
                    history.back(), $.msg.close(index);
                });
            };

        }
    })
</script></div>